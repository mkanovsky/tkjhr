<?php

use yii\db\Migration;

class m160619_192222_item_image_a_item_popis extends Migration
{
    public function up()
    {
        $q = "ALTER TABLE `item`
ADD `text` mediumtext NULL,
ADD `imagePath` varchar(250) NULL AFTER `text`,
ADD `thumbnailPath` varchar(250) NULL AFTER `imagePath`,
COMMENT='';";
        
        \Yii::$app->db->createCommand($q)->execute();
    }

    public function down()
    {
        echo "m160619_192222_item_image_a_item_popis cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
