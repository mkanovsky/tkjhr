<?php

use yii\db\Migration;

class m160611_152615_item_type extends Migration
{
    public function up()
    {
        $q = "
        CREATE TABLE `item_type` (
          `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
          `name` varchar(512) NOT NULL
        ) COMMENT='';
        ";
        Yii::$app->db->createCommand($q)->execute();
    }

    public function down()
    {
        echo "m160611_152615_item_type cannot be reverted.\n";
        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
