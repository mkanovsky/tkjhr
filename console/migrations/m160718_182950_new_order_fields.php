<?php

use yii\db\Migration;

class m160718_182950_new_order_fields extends Migration
{
    public function up()
    {
        $q = "ALTER TABLE `order`
ADD `street` varchar(50) COLLATE 'utf8_general_ci' NOT NULL,
ADD `zip` varchar(50) COLLATE 'utf8_general_ci' NOT NULL AFTER `street`,
ADD `city` varchar(50) COLLATE 'utf8_general_ci' NOT NULL AFTER `zip`,
ADD `otherStreet` varchar(50) COLLATE 'utf8_general_ci' NOT NULL AFTER `city`,
ADD `otherZip` varchar(50) COLLATE 'utf8_general_ci' NOT NULL AFTER `otherStreet`,
ADD `otherCity` varchar(50) COLLATE 'utf8_general_ci' NOT NULL AFTER `otherZip`,
ADD `phone` varchar(50) COLLATE 'utf8_general_ci' NOT NULL AFTER `otherCity`;";


        \Yii::$app->db->createCommand($q)->execute();
    }

    public function down()
    {
        echo "m160718_182950_new_order_fields cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
