<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Handles adding new_field to table `user`.
 */
class m160531_115102_add_new_field_to_user extends Migration
{
    public function up()
    {
        $this->addColumn('{{%user}}', 'idKarty', schema::TYPE_STRING);
		$this->addColumn('{{%user}}', 'cisloOP', schema::TYPE_STRING);
		$this->addColumn('{{%user}}', 'adresa', schema::TYPE_STRING);
    }

    public function down()
    {
		$this->dropColumn('{{%user}}', 'idKarty', schema::TYPE_STRING);
		$this->dropColumn('{{%user}}', 'cisloOP', schema::TYPE_STRING);
		$this->dropColumn('{{%user}}', 'adresa', schema::TYPE_STRING);
    }
}
