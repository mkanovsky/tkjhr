<?php

use yii\db\Migration;

class m160611_161224_userRelations extends Migration
{
    public function up()
    {
        $q = "
        ALTER TABLE `order`
        CHANGE `customerId` `userId` bigint NOT NULL AFTER `dateTo`,
        COMMENT='';
        ";
        Yii::$app->db->createCommand($q)->execute();
    }

    public function down()
    {
        echo "m160611_161224_userRelations cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
