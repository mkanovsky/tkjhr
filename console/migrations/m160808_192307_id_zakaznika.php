<?php

use yii\db\Migration;

class m160808_192307_id_zakaznika extends Migration
{
    public function up()
    {

        $q = "ALTER TABLE `profile`
ADD `number` varchar(10) NULL";

        \Yii::$app->db->createCommand($q)->execute();

    }

    public function down()
    {
        echo "m160808_192307_id_zakaznika cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
