<?php

use yii\db\Migration;

class m160712_181559_vacsi_obrazok extends Migration
{
    public function up()
    {
        
        $q = "ALTER TABLE `item`
ADD `detailThumbnailPath` varchar(250) NULL AFTER `imagePath`,
COMMENT='';";
        
        \Yii::$app->db->createCommand($q)->execute();
    }

    public function down()
    {
        echo "m160712_181559_vacsi_obrazok cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
