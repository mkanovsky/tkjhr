<?php

use yii\db\Migration;

class m160605_144139_basic_app_structure extends Migration
{
    public function up()
    {
        $q = "
        CREATE TABLE `customer` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `userId` int(11) NOT NULL,
          PRIMARY KEY (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;


        CREATE TABLE `item` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `typeId` int(11) NOT NULL,
          `stock` int(11) NOT NULL DEFAULT '0',
          `price` decimal(10,2) NOT NULL,
          PRIMARY KEY (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;


        CREATE TABLE `order` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `totalPrice` decimal(10,2) NOT NULL,
          `dateAdded` datetime NOT NULL,
          `status` varchar(50) NOT NULL,
          `dateFrom` datetime NOT NULL,
          `dateTo` datetime NOT NULL,
          `customerId` int(11) NOT NULL,
          `deliveryType` varchar(50) NOT NULL,
          PRIMARY KEY (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;


        CREATE TABLE `order_item` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `orderId` int(11) NOT NULL,
          `itemId` int(11) NOT NULL,
          `price` decimal(10,2) NOT NULL,
          PRIMARY KEY (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ";

        Yii::$app->db->createCommand($q)->execute();
    }

    public function down()
    {
        echo "m160605_144139_basic_app_structure cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
