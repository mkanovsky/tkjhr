<?php

use yii\db\Migration;

class m160727_123310_cena3 extends Migration
{
    public function up()
    {
        $q = "
        ALTER TABLE `item`
        CHANGE `price` `price` decimal(10,2) NOT NULL COMMENT 'normalna cena' AFTER `stock`,
        CHANGE `price2` `price2` decimal(10,2) NULL COMMENT 'cena za 7d+' AFTER `price`,
        ADD `price3` decimal(10,2) NULL COMMENT 'cena po 22:00' AFTER `price2`;
        ";
        \Yii::$app->db->createCommand($q)->execute();
    }

    public function down()
    {
        echo "m160727_123310_cena3 cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
