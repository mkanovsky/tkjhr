<?php

use yii\db\Migration;

class m160709_173600_item_type_update extends Migration
{
    public function up()
    {
        $q = "-- Adminer 4.2.5 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `item_type`;
CREATE TABLE `item_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Unique tree node identifier',
  `root` int(11) DEFAULT NULL COMMENT 'Tree root identifier',
  `lft` int(11) NOT NULL COMMENT 'Nested set left property',
  `rgt` int(11) NOT NULL COMMENT 'Nested set right property',
  `lvl` smallint(5) NOT NULL COMMENT 'Nested set level / depth',
  `name` varchar(60) NOT NULL COMMENT 'The tree node name / label',
  `icon` varchar(255) DEFAULT NULL COMMENT 'The icon to use for the node',
  `icon_type` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Icon Type: 1 = CSS Class, 2 = Raw Markup',
  `active` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Whether the node is active (will be set to false on deletion)',
  `selected` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Whether the node is selected/checked by default',
  `disabled` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Whether the node is enabled',
  `readonly` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Whether the node is read only (unlike disabled - will allow toolbar actions)',
  `visible` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Whether the node is visible',
  `collapsed` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Whether the node is collapsed by default',
  `movable_u` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Whether the node is movable one position up',
  `movable_d` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Whether the node is movable one position down',
  `movable_l` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Whether the node is movable to the left (from sibling to parent)',
  `movable_r` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Whether the node is movable to the right (from sibling to child)',
  `removable` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Whether the node is removable (any children below will be moved as siblings before deletion)',
  `removable_all` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Whether the node is removable along with descendants',
  PRIMARY KEY (`id`),
  KEY `item_type_NK1` (`root`),
  KEY `item_type_NK2` (`lft`),
  KEY `item_type_NK3` (`rgt`),
  KEY `item_type_NK4` (`lvl`),
  KEY `item_type_NK5` (`active`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `item_type` (`id`, `root`, `lft`, `rgt`, `lvl`, `name`, `icon`, `icon_type`, `active`, `selected`, `disabled`, `readonly`, `visible`, `collapsed`, `movable_u`, `movable_d`, `movable_l`, `movable_r`, `removable`, `removable_all`) VALUES
(1, 1,  1,  14, 0,  'Playstation 4',    '', 1,  1,  0,  0,  0,  1,  0,  1,  1,  1,  1,  1,  0),
(2, 2,  1,  6,  0,  'XBOX One', '', 1,  1,  0,  0,  0,  1,  0,  1,  1,  1,  1,  1,  0),
(3, 1,  2,  11, 1,  'Hry',  '', 1,  1,  0,  0,  0,  1,  0,  1,  1,  1,  1,  1,  0),
(4, 1,  12, 13, 1,  'Ovládače', '', 1,  1,  0,  0,  0,  1,  0,  1,  1,  1,  1,  1,  0),
(5, 2,  2,  3,  1,  'Hry',  '', 1,  1,  0,  0,  0,  1,  0,  1,  1,  1,  1,  1,  0),
(6, 2,  4,  5,  1,  'Ovládače', '', 1,  1,  0,  0,  0,  1,  0,  1,  1,  1,  1,  1,  0),
(7, 1,  3,  4,  2,  'Akčné',    '', 1,  1,  0,  0,  0,  1,  0,  1,  1,  1,  1,  1,  0),
(8, 1,  5,  6,  2,  'Športové', '', 1,  1,  0,  0,  0,  1,  0,  1,  1,  1,  1,  1,  0),
(9, 1,  7,  8,  2,  'Simulátory',   '', 1,  1,  0,  0,  0,  1,  0,  1,  1,  1,  1,  1,  0),
(10,    1,  9,  10, 2,  'RPG',  '', 1,  1,  0,  0,  0,  1,  0,  1,  1,  1,  1,  1,  0);

-- 2016-07-09 17:38:13

            ";

    \Yii::$app->db->createCommand($q)->execute();
    }

    public function down()
    {
        echo "m160709_173600_item_type_update cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
