<?php

use yii\db\Migration;

class m160707_151409_item_parent extends Migration
{
    public function up()
    {
        $q = "ALTER TABLE `item_type`
ADD `parentId` int(11) NOT NULL DEFAULT '0';";

        \Yii::$app->db->createCommand($q)->execute();
    }

    public function down()
    {
        echo "m160707_151409_item_parent cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
