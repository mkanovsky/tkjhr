<?php

use yii\db\Migration;

class m160614_211240_user_fields extends Migration
{
    public function up()
    {
        $q = "
        ALTER TABLE `profile`
ADD `street` varchar(120) NOT NULL,
ADD `zip` varchar(120) NOT NULL AFTER `street`,
ADD `city` varchar(120) NOT NULL AFTER `zip`,
ADD `name` varchar(120) NOT NULL AFTER `city`,
ADD `surname` varchar(120) NOT NULL AFTER `name`,
ADD `ssn` varchar(12) NOT NULL AFTER `surname`,
ADD `idNumber` varchar(12) NOT NULL AFTER `ssn`,
COMMENT=''; ";
        \Yii::$app->db->createCommand($q)->execute();
    }

    public function down()
    {
        echo "m160614_211240_user_fields cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
