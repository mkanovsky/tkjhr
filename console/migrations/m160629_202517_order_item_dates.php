<?php

use yii\db\Migration;

class m160629_202517_order_item_dates extends Migration
{
    public function up()
    {
        $q = "ALTER TABLE `order_item`
ADD `dateFrom` datetime NULL,
ADD `dateTo` datetime NULL AFTER `dateFrom`;";
        \Yii::$app->db->createCommand($q)->execute();
    }

    public function down()
    {
        echo "m160629_202517_order_update cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
