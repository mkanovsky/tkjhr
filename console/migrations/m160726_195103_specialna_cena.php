<?php

use yii\db\Migration;

class m160726_195103_specialna_cena extends Migration
{
    public function up()
    {
        $q = "ALTER TABLE `item`
            ADD `price2` decimal(10,2) NULL AFTER `price`;";

        \Yii::$app->db->createCommand($q)->execute();
    }

    public function down()
    {
        echo "m160726_195103_specialna_cena cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
