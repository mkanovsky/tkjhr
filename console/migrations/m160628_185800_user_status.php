<?php

use yii\db\Migration;

class m160628_185800_user_status extends Migration
{
    public function up()
    {
        $q = "ALTER TABLE `user`
ADD `status` int(4) NOT NULL DEFAULT 0;";

        \Yii::$app->db->createCommand($q)->execute();

        \Yii::$app->db->createCommand("UPDATE user SET status = 10")->execute();        
    }

    public function down()
    {
        echo "m160628_185800_user_status cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
