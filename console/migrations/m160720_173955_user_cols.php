<?php

use yii\db\Migration;

class m160720_173955_user_cols extends Migration
{
    public function up()
    {
        $q = "
            ALTER TABLE `profile`
ADD `otherStreet` varchar(120) COLLATE 'utf8_unicode_ci' NOT NULL AFTER `city`,
ADD `otherZip` varchar(120) COLLATE 'utf8_unicode_ci' NOT NULL AFTER `otherStreet`,
ADD `otherCity` varchar(120) COLLATE 'utf8_unicode_ci' NOT NULL AFTER `otherZip`;
        ";

        \Yii::$app->db->createCommand($q)->execute();
    }

    public function down()
    {
        echo "m160720_173955_user_cols cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
