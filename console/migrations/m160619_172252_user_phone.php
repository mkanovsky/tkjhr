<?php

use yii\db\Migration;

class m160619_172252_user_phone extends Migration
{
    public function up()
    {
        $q = "ALTER TABLE `user`
ADD `phone` varchar(12) COLLATE 'utf8_unicode_ci' NOT NULL,
COMMENT='';";
        \Yii::$app->db->createCommand($q)->execute();
    }

    public function down()
    {
        echo "m160619_172252_user_phone cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
