<?php

use yii\db\Migration;

class m160611_154854_item_name extends Migration
{
    public function up()
    {
        $q = "ALTER TABLE `item`
        ADD `name` varchar(512) NOT NULL AFTER `typeId`,
        COMMENT='';";

        Yii::$app->db->createCommand($q)->execute();
    }

    public function down()
    {
        echo "m160611_154854_item_name cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
