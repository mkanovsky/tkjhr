<?php

use yii\db\Migration;

class m160726_153819_rozvoz_cena extends Migration
{
    public function up()
    {
        $q = "CREATE TABLE `delivery_price` (
          `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
          `name` varchar(255) NULL,
          `amount` decimal(10,2) NULL
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

        \Yii::$app->db->createCommand($q)->execute();

        $r = [
            'Staré mesto' => 3,
            'Nové mesto' => 3,
            'Ružinov' => 3,
            'Prievoz' => 3,
            'Petržálka' => 3,
            'Karlova Ves' => 3,
            'Vrakuňa' => 5,
            'Podunajské biskupice' => 5,
            'Rača' => 5,
            'Devínska Nová Ves' => 5,
            'Dúbravka' => 5,
            'Lamač' => 5,
            'Ivanka pri Dunaji' => 7,
            'Zálesie' => 7,
            'Malinovo' => 7,
            'Most pri Bratislave' => 7,
            'Dunajská Lužná' => 7,
            'Rovinka' => 7,
            'Maránka' => 7,
            'Svätý Júr' => 7,
            'Kittsee' => 7,
        ];


        foreach ($r as $mesto=>$cena) {
            $q = "INSERT INTO delivery_price (name, amount) VALUES ('$mesto', $cena)";
            \Yii::$app->db->createCommand($q)->execute();
        }
    }

    public function down()
    {
        echo "m160726_153819_rozvoz_cena cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
