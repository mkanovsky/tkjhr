<?php

use yii\db\Migration;

class m160612_133005_orderitem_count extends Migration
{
    public function up()
    {
        $q = "ALTER TABLE `order_item`
        ADD `count` int NULL DEFAULT '1',
        COMMENT='';";

        Yii::$app->db->createCommand($q)->execute();
    }

    public function down()
    {
        echo "m160612_133005_orderitem_count cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
