<?php

use yii\db\Migration;

class m160619_173624_profile_update extends Migration
{
    public function up()
    {
        $q = "
        ALTER TABLE `profile`
ADD `street` varchar(120) NOT NULL,
ADD `zip` varchar(120) NOT NULL AFTER `street`,
ADD `city` varchar(120) NOT NULL AFTER `zip`,
ADD `ssn` varchar(12) NOT NULL AFTER `city`,
ADD `idNumber` varchar(12) NOT NULL AFTER `ssn`,
COMMENT=''; ";
        \Yii::$app->db->createCommand($q)->execute();

         $q = "ALTER TABLE `profile`
ADD `phone` varchar(12) COLLATE 'utf8_unicode_ci' NOT NULL,
COMMENT='';";
        \Yii::$app->db->createCommand($q)->execute();

        \Yii::$app->db->createCommand("
            ALTER TABLE `user`
DROP `street`,
DROP `zip`,
DROP `city`,
DROP `name`,
DROP `surname`,
DROP `ssn`,
DROP `idNumber`,
DROP `phone`,
COMMENT='';
        ")->execute();
    }

    public function down()
    {
        echo "m160619_173624_profile_update cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
