<?php

use yii\db\Migration;

class m160726_200827_specialna_cena_1 extends Migration
{
    public function up()
    {
        $q = "ALTER TABLE `order_item`
            ADD `price2` decimal(10,2) NULL AFTER `price`;";

        \Yii::$app->db->createCommand($q)->execute();
    }

    public function down()
    {
        echo "m160726_200827_specialna_cena_1 cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
