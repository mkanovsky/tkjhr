<?php

use yii\db\Migration;

class m160630_200045_user_admin_role extends Migration
{
    public function up()
    {
        $q = "ALTER TABLE `user`
        ADD `role` int(4) NOT NULL DEFAULT '0'";

        \Yii::$app->db->createCommand($q)->execute();
 
    }

    public function down()
    {
        echo "m160630_200045_user_admin_role cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
