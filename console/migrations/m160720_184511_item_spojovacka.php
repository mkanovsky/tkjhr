<?php

use yii\db\Migration;

class m160720_184511_item_spojovacka extends Migration
{
    public function up()
    {
        $q = "CREATE TABLE `item_has_type` (
              `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
              `itemId` int NOT NULL,
              `itemTypeId` int NOT NULL
        );";

        \Yii::$app->db->createCommand($q)->execute();
    }

    public function down()
    {
        echo "m160720_184511_item_spojovacka cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
