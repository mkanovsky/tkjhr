<?php

use yii\db\Migration;

class m160720_174344_memo extends Migration
{
    public function up()
    {
        $q = "ALTER TABLE `order`
CHANGE `street` `street` varchar(50) COLLATE 'utf8_general_ci' NULL AFTER `deliveryType`,
CHANGE `zip` `zip` varchar(50) COLLATE 'utf8_general_ci' NULL AFTER `street`,
CHANGE `city` `city` varchar(50) COLLATE 'utf8_general_ci' NULL AFTER `zip`,
ADD `memo` text COLLATE 'utf8_general_ci' NULL;";

        \Yii::$app->db->createCommand($q)->execute();
    }

    public function down()
    {
        echo "m160720_174344_memo cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
