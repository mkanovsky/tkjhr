<?php 
namespace console\controllers;

class TestController extends \yii\console\Controller
{	

	public function actionTest() {

		// give in DB IS :
		// one ordered Item with id 1, orderdd from 2016-06-01 to 2016-06-10 and the total stock count is 3
		$this->testBookedCount();
	}	


	private function testBookedCount() {
		$true = 0;

		$x = \common\models\Item::findOne(1);
		$c = $x->bookedCount('2016-06-07', '2016-06-20');
		if ($c == 1) $true++;

		$x = \common\models\Item::findOne(1);
		$c = $x->bookedCount('2016-05-07', '2016-06-05');
		if ($c == 1) $true++;

		$x = \common\models\Item::findOne(1);
		$c = $x->bookedCount('2016-06-02', '2016-06-05');
		if ($c == 1) $true++;

		$x = \common\models\Item::findOne(1);
		$c = $x->bookedCount('2016-05-05', '2016-06-20');
		if ($c == 1) $true++;

		echo $true; // 4
	}

	public function actionEmail() {
		$order = \common\models\Order::findOne(1);

		$order->sendMail('kanovskym@gmail.com');
	}

}  