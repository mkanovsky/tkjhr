<?php

namespace common\components;

use Yii;
use yii\data\ActiveDataProvider;

/**
 */
class Helpers
{

	public static function cuteNumber($n) {
		return number_format($n, 2, ',', ' ');
	}
	
	public static function dateDiff($d1, $d2) {
		$datetime1 = date_create($d1);
		$datetime2 = date_create($d2);
		$interval = date_diff($datetime1, $datetime2);
		return (int)$interval->format('%R%a');
	}

	public static function dateDiff2($d1, $d2) {
		return ceil( (strtotime($d2) - strtotime($d1))/86400 );

		$datetime1 = date_create($d1);
		$datetime2 = date_create($d2);
		$interval = date_diff($datetime1, $datetime2);
		return (int)$interval->format('%R%a');
	}

	/**
	* Obtains an object class name without namespaces
	*/
	public static function get_real_class($obj) {
	    $classname = get_class($obj);

	    if (preg_match('@\\\\([\w]+)$@', $classname, $matches)) {
	        $classname = $matches[1];
	    }

	    return $classname;
	}
	
	public static function formatDateFromDb($date, $noTime = false) {
	   $date = trim($date);
	   $format = 'd.m.Y';
		if (strlen($date) > 10) {

			if (!$noTime)
				$format .= ' H:i';

		}
		$time = strtotime($date);
		if ($time)
			$date = date($format, $time);
		else $date = '';
		return $date;
	}

	public static function formatDateToDb($date) {
		$date = trim($date);
		$dateArray = explode(' ', $date);
		if (count($dateArray) == 1) {
			$date = $date.' 00:00';
		} 

		$object = \DateTime::createFromFormat('d.m.Y H:i', $date);

		if ($object) {
			$time = $object->getTimestamp();
			return date('Y-m-d H:i:s', $time);
		} else {
			return false;
		}
	}

	public static function addDaysToDate($date, $days) {
		return date('Y-m-d', strtotime($date) + $days*86400);
	}

	public static function getIbanFromBban($bankAccount='',$bankAccountCode='', $bankAccountPrefix = '000000') {

        if ($bankAccount=='' || $bankAccountCode=='') {
            return false;
        }

        if (trim($bankAccountPrefix) == '') {
        	$bankAccountPrefix = '000000';
        }
		
		$iban = 'SK00'.$bankAccountCode.$bankAccountPrefix.str_pad($bankAccount,10,'0',STR_PAD_LEFT);
		
		include_once(__DIR__.'/../extensions/iban/php-iban.php');

        return iban_set_checksum($iban);
    
    }

    public static function stringContains($needle, $haystack) {

        if (strpos($haystack, $needle) !== false) {
            return true;
        }

        return false;
    }

    public static function getDateOfBirthFromSsn($ssn, $format = "normal") {

		if (strlen($ssn) < 8) {
			return false;
		}

		$year = substr($ssn, 0, 2);
		$month = substr($ssn, 2,2);
		$day = substr($ssn, 4, 2);

		if (self::getGenderFromSsn($ssn) == 'F') {
			$month = sprintf("%02d", $month-50);
		}
		
		if ($year < 20) $year = 2000 + $year;
		if ($year >= 20) $year = 1900 + $year;

		if ($format = "normal") {
			return $date = $day.".".$month.".".$year;
		} elseif ($format = "db") {
			return $year.'-'.$month.'-'.$day;
		} else {
			return false;
		}

	}

	public static function getGenderFromSsn($ssn) {
			
		$thirdLetter = substr($ssn, 2, 1);
		
		if ($thirdLetter !== FALSE && $thirdLetter !== '')
			return (int)$thirdLetter > 3 ? 'F' : 'M';
		else return '?';

	}

	public static function getAgeFromSsn($ssn) {

		$date = self::getDateOfBirthFromSsn($ssn, $format = "db");
		$time = strtotime($date);

		$today = strtotime(date('Y-m-d'));

		$age = floor(  ($today - $time) / 86400  / 365);
		
		return $age;
	}

	public static function generatePassword($length = 8) {
	    $chars = 'abcdefghijkmnopqrstuvwx23456789';
	    $count = mb_strlen($chars);

	    for ($i = 0, $result = ''; $i < $length; $i++) {
	        $index = rand(0, $count - 1);
	        $result .= mb_substr($chars, $index, 1);
	    }

	    return $result;
	}

	public static function generateNumber($length = 8) {
	    $chars = '1234567890';
	    $count = mb_strlen($chars);

	    for ($i = 0, $result = ''; $i < $length; $i++) {
	        $index = rand(0, $count - 1);
	        $result .= mb_substr($chars, $index, 1);
	    }

	    return $result;
	}

}