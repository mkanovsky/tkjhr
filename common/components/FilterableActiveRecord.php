<?php 

namespace common\components;

use yii\data\ActiveDataProvider;
use yii\base\NotSupportedException;
use yii\db\ActiveRecord;
use Yii;


class FilterableActiveRecord extends ActiveRecord
{

    protected $visibleColumns = array();
    protected $relations = array();

    /**
      * @inheritdoc
      */
    public function rules()
    {
        return [
           [$this->safeAttributes(), 'safe'],
        ];
    }

    public function safeAttributes() {
        
        $settings = $this->getGridSettings();
      
        $safe = [];
        foreach($settings as $col) {
            $safe[] = $col['attribute'];
        }
        return $safe;
    }

    public function setVisibleColumns($columns) {
        $return = $this->getGridSettings();

        $r = array();

        foreach ($columns as $key=>$value) {
            if (!is_array($value) && isset($return[$value])) {
                $r[$value] = $return[$value];
            } else {
                if (isset($columns[$key]['relation'])) {
                    $this->relations[$key] = $columns[$key]['relation'];

                    if (isset($this->relations[$key]['type'])) 
                        {
                            switch($this->relations[$key]['type']) {
                                case 'date' :
                                    $settings[$key] = [
                                        'attribute' => $key,
                                        'value' => $columns[$key]['value'],
                                        'class' => \kartik\grid\DataColumn::className(),
                                        'format' => ['date', 'php:d.m.Y'],
                                        'filterType' => \kartik\grid\GridView::FILTER_DATE_RANGE,
                                        'filterWidgetOptions' => [
                                            'convertFormat'=>true,
                                            'pluginOptions'=> 
                                                [
                                                    'format' =>'d.m.Y'
                                                ],
                                        ]
                                    ];
                                break;

                                case 'datetime' :
                                    $settings[$key] = [
                                        'attribute' => $key,
                                        'value' => $columns[$key]['value'],
                                        'class' => \kartik\grid\DataColumn::className(),
                                        'format' => ['date', 'php:d.m.Y'],
                                        'filterType' => \kartik\grid\GridView::FILTER_DATE_RANGE,
                                        'filterWidgetOptions' => [
                                            'convertFormat'=>true,
                                            'pluginOptions'=> 
                                                [
                                                    'format' =>'d.m.Y'
                                                ],
                                        ]
                                    ];
                                break;

                                case 'integer'  :
                                    $settings[$key] = [
                                        'attribute' => $key,
                                        'value' => $columns[$key]['value'],
                                        'class' => \kartik\grid\DataColumn::className(),
                                        // 'format' => ['decimal', 2],
                                    ];
                                break;

                                case 'decimal'  :
                                    $settings[$key] = [
                                        'attribute' => $key,
                                        'value' => $columns[$key]['value'],
                                        'class' => \kartik\grid\DataColumn::className(),
                                        'format' => ['decimal', 2],
                                    ];
                                break;

                                default:
                                    $settings[$key] = [
                                        'attribute' => $key,
                                        'value' => $columns[$key]['value'],
                                        'class' => \kartik\grid\DataColumn::className()
                                    ];
                                break;
                            }
                            $r[$key] = $settings[$key];
                        } else {
                            $r[$key] = $columns[$key];
                        }

                    unset($columns[$key]['relation']);
                }
                
            }
        }

        $this->visibleColumns = $r;

    }

    public function getVisibleColumns() {
        if (empty($this->visibleColumns)) {
            $this->visibleColumns = $this->getGridSettings();
        }
        return $this->visibleColumns;
    }

    public function getGridSettings() {

        $schema = $this->getTableSchema();
        $settings = [];

        foreach ($schema->columns as $col) {
            switch ($col->type) {

                case 'date' :
                    $settings[$col->name] = [
                        'attribute' => $col->name,
                        'class' => \kartik\grid\DataColumn::className(),
                        'format' => ['date', 'php:d.m.Y'],
                        'filterType' => \kartik\grid\GridView::FILTER_DATE_RANGE,
                        'filterWidgetOptions' => [
                            'convertFormat'=>true,
                            'pluginOptions'=> 
                                [
                                    'format' =>'d.m.Y'
                                ],
                        ]
                    ];
                break;

                case 'datetime' :
                    $settings[$col->name] = [
                        'attribute' => $col->name,
                        'class' => \kartik\grid\DataColumn::className(),
                        'format' => ['date', 'php:d.m.Y'],
                        'filterType' => \kartik\grid\GridView::FILTER_DATE_RANGE,
                        'filterWidgetOptions' => [
                            'convertFormat'=>true,
                            'pluginOptions'=> 
                                [
                                    'format' =>'d.m.Y'
                                ],
                        ]
                    ];
                break;

                case 'integer'  :
                        $settings[$col->name] = [
                        'attribute' => $col->name,
                        'class' => \kartik\grid\DataColumn::className(),
                        // 'format' => ['decimal', 2],
                    ];
                break;

                case 'decimal'  :
                        $settings[$col->name] = [
                        'attribute' => $col->name,
                        'class' => \kartik\grid\DataColumn::className(),
                        'format' => ['decimal', 2],
                    ];
                break;


                default:
                    // die();
                    if ($col->enumValues) {

                        $settings[$col->name] = [
                            'attribute' => $col->name,
                            'class' => \kartik\grid\DataColumn::className(),
                            'filterType'=>\kartik\grid\GridView::FILTER_SELECT2,
                            'filter'=>array_combine($col->enumValues, $col->enumValues),
                             'filterWidgetOptions'=>[
                                'pluginOptions'=>['allowClear'=>true],
                            ],
                            'filterInputOptions'=>['placeholder'=>'Any '.$col->name],
                        ];
                    } else {
                        $settings[$col->name] = [
                            'attribute' => $col->name,
                            'class' => \kartik\grid\DataColumn::className(),
                        ];
                    }

                break;

            }
        }


        return $settings;

    }

    public function search($params) {

        
        $query = static::find();


        if ($this->load($params)) {
            
            $properties = [];
            foreach ($this->getTableSchema()->columns as $column) {
                if ($this->{$column->name})
                switch ($column->type) {

                    case 'date':
                        $dates = explode(' - ', $this->{$column->name});
                        $fromDate = \common\components\Helpers::formatDateToDb($dates[0]);
                        $toDate = \common\components\Helpers::formatDateToDb($dates[1]);

                        // $subQ[$column->name]['betxween'] = [$fromDate,$toDate];
                        $query->andWhere(['between', $column->name, $fromDate, $toDate]);
                    break;

                    case 'datetime':
                        $dates = explode(' - ', $this->{$column->name});
                        $fromDate = \common\components\Helpers::formatDateToDb($dates[0]);
                        $toDate = \common\components\Helpers::formatDateToDb($dates[1]);

                        // $subQ[$column->name]['betxween'] = [$fromDate,$toDate];
                        $query->andWhere(['between', $column->name, $fromDate, $toDate]);
                    break;

                    case 'decimal':
                        if (\common\components\Helpers::stringContains('&', $this->{$column->name})) {
                            $args = explode('&', $this->{$column->name});
                            
                            $query->andWhere($column->name.' '.trim($args[0]));
                            $query->andWhere($column->name.' '.trim($args[1]));
                        } else {

                            $query->andWhere($column->name.' '.$this->{$column->name});
                        }

                    break;

                    case 'integer':
                        if (\common\components\Helpers::stringContains('&', $this->{$column->name})) {
                            $args = explode('&', $this->{$column->name});
                            
                            $query->andWhere($column->name.' '.trim($args[0]));
                            $query->andWhere($column->name.' '.trim($args[1]));
                        } else {

                            $query->andWhere($this->tableName().'.'.$column->name.'='.$this->{$column->name});
                        }

                    break;

                    default:
                        $properties[$column->name] = $this->{$column->name};
                    break;
                }

            } 

            $parameters = $params[self::formName()];
            $vc = $this->getVisibleColumns();

            foreach ($this->relations as $name=>$options) {
                if (isset($vc[$name]['value']) && isset($parameters[$name])) {
                    $this->$name = $parameters[$name];
                    $properties[$vc[$name]['value']] = $parameters[$name];
                }
            }

            $query->andFilterWhere(
                $properties
            );
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        $sortAttributes = $dataProvider->getSort()->attributes;

        foreach ($this->relations as $name=>$options) {
            if (isset($options['join'])) {
                $dataProvider->query->join[] = $options['join'];
            }

            if (isset($options['sort'])) {
                $sortAttributes[$name] = $options['sort'];
            }

        }


        $dataProvider->setSort([
            'attributes' => $sortAttributes
                
        ]);


        return $dataProvider;
        
    }
}