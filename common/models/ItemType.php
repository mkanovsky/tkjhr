<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "item_type".
 *
 * @property integer $id
 * @property string $name
 */
class ItemType extends \kartik\tree\models\Tree
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'item_type';
    }

    public function getFullName() {
        return $this->name;
    }

    public function getItems() {
        return $this->hasMany(Item::className(), ['id' => 'itemId'])
          ->viaTable('item_has_type', ['typeId' => 'id']);
    }

  
}
