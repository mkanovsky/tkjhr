<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**

 */
class OrderItem extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order_item';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['price', 'price2', 'count', 'itemId', 'orderId', 'dateFrom', 'dateTo'], 'safe'],
        ];
    }

    public function getItem() {
        return $this->hasOne(Item::className(), ['id' => 'itemId']);
    }
}
