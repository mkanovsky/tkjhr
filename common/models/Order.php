<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use \dektrium\user\models\User;

/**

 */
class Order extends ActiveRecord
{

    public $otherDeliveryPriceId;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
        ];
    }

    public static function getStatuses() {
        $statuses = [
            'new' => 'Nová',
            'sent' => 'Odovzdaná',
            'closed' => 'Vrátená / dokončená',
            'cancelled' => 'Zrušená'
        ];
        return $statuses;
    }

    public static function getDeliveryTypes() {
        $statuses = [
            'onplace' => 'osobne',
            'courier' => 'kuriérom',
        ];
        
        return $statuses;
    }

    public function updateTotalPrice() {
        
        $sum = 0;

        $data = Yii::$app->db->createCommand("
            SELECT 
                id, dateFrom, dateTo, itemId, price, price2, count, ceil(TIMESTAMPDIFF(MINUTE,dateFrom, dateTo)/60/24) as days
            FROM order_item oi WHERE orderId = {$this->id}
        ")->queryAll();
        
        foreach ($data as $row) { 
            $sum = $sum+$row['price'];
        }

        $this->totalPrice = $sum;

        if ($this->deliveryType == 'courier') {
            $this->totalPrice += $this->deliveryPrice;
        }

        $this->save(false, array('totalPrice'));
        return $this->totalPrice;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userId', 'status','dateFrom', 'dateTo'], 'required'],
            [['userId'], 'integer'],
            // [['price'], 'number'],
            [['dateFrom', 'dateTo', 'deliveryType', 'status', 'memo', 'deliveryPrice', 'deliveryPriceId'], 'safe'],
            [['otherStreet','otherZip','otherCity','phone', 'dateAdded'], 'safe'],
            [['street','zip','city', 'deliveryType'], 'required']
        ];
    }

    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'userId']);
    }

    public function getOrderItems() {
        return $this->hasMany(OrderItem::className(), ['orderId' => 'id']);   
    }

    public function hasOtherAddress() {
        return $this->otherStreet || $this->otherZip || $this->otherCity;
    }

    public function sendMail($to) {

        $mailer = \Yii::$app->mailer;
        $mailer->viewPath;
        // echo $mailer->viewPath;die();
        $mailer->getView()->theme = Yii::$app->view->theme;

        $subject = 'tkjhr.sk - Objednávka číslo '.$this->id;
        
        $view = 'newOrder';

        $params['order']=$this;

        return $mailer->compose(['html' => $view, 'text' => 'text/' . $view], $params)
            ->setTo($to)
            ->setBcc(array('tkjhr@tkjhr.sk', 'kanovskym@gmail.com'))
            ->setFrom(Yii::$app->params['adminEmail'])
            ->setSubject($subject)
            ->send();
    }
}
