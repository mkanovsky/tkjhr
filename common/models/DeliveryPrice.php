<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "delivery_price".
 *
 * @property integer $id
 * @property string $name
 * @property string $amount
 */
class DeliveryPrice extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'delivery_price';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['amount'], 'number'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'amount' => 'Amount',
        ];
    }
    
}
