<?php

namespace common\models;

use Yii;
use yii\imagine\Image;

/**
 * This is the model class for table "item".
 *
 * @property integer $id
 * @property integer $typeId
 * @property integer $stock
 * @property string $price
 */
class Item extends \yii\db\ActiveRecord
{

    const DUALSHOCK_ID = 44;
    const XBOXCONTROLLER_ID = 45;

    public $imageFile;

    // these are for searching
    public $dateFrom;
    public $dateTo;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'item';
    }

    public function isPsKonzola() {
        return !!\Yii::$app->db->createCommand('SELECT itemId FROM item_has_type WHERE itemId = '.$this->id.' AND itemTypeId = 11')->queryScalar();
    }

    public function isXboxKonzola() {
        return !!\Yii::$app->db->createCommand('SELECT itemId FROM item_has_type WHERE itemId = '.$this->id.' AND itemTypeId = 12')->queryScalar();
    }

    public function isPsOvladac() {
        return !!\Yii::$app->db->createCommand('SELECT itemId FROM item_has_type WHERE itemId = '.$this->id.' AND itemTypeId = 4')->queryScalar();
    }

    public function isXboxOvladac() {
        return !!\Yii::$app->db->createCommand('SELECT itemId FROM item_has_type WHERE itemId = '.$this->id.' AND itemTypeId = 6')->queryScalar();
    }
    
    public function isKonzola() {
        return $this->isXboxKonzola() || $this->isPsKonzola();
    }
    
    public function isOvladac() {
        return $this->isXboxOvladac() || $this->isPsOvladac();
    }

    public function isHra() {
        return ! ($this->isKonzola() || $this->isOvladac());
    }

    public static function findPsOvladac() {
        return self::findOne(self::DUALSHOCK_ID);
    }

    public static function findXboxOvladac() {
        return self::findOne(self::XBOXCONTROLLER_ID);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['price', 'stock'], 'required'],
            [['stock'], 'integer'],
            [['price'], 'number'],
            [['name', 'imagePath', 'thumbnailPath', 'detailThumbnailPath', 'text'], 'safe'],
            [['imageFile'], 'file', 'checkExtensionByMimeType'=>false, 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
            [['dateFrom', 'dateTo', 'price2', 'price3'], 'safe'],
            // [['dateFrom', 'dateTo'], 'safe' => true],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'typeId' => Yii::t('app', 'Typ'),
            'stock' => Yii::t('app', 'Počet na sklade'),
            'price' => Yii::t('app', 'Cena za 24/h'),
            'price2' => Yii::t('app', 'Cena za 24/h 7 dni a viac'),
            'price3' => Yii::t('app', 'Cena po 22:00 prvy den'),
            'name' => Yii::t('app', 'Názov'),
        ];
    }

    public function getType() {
        return $this->hasOne(ItemType::className(), ['id' => 'typeId']);
    }

    public function getTypes() {
        return $this->hasMany(ItemType::className(), ['id' => 'itemTypeId'])
          ->viaTable('item_has_type', ['itemId' => 'id']);
    }

    public function bookedCount($dateFrom, $dateTo) {
        // '2016-06-10' '2016-06-20'
        // v db je '2016-06-05' '2016-06-09'
        // takze vratim nic

        $q = "
            SELECT sum(count) FROM order_item oi
            LEFT JOIN `order` o ON o.id = oi.orderId
            WHERE 
                (
                  (date('$dateFrom')  BETWEEN oi.dateFrom AND oi.dateTo) OR 
                  (date('$dateTo')  BETWEEN oi.dateFrom AND oi.dateTo) OR 
                  ('$dateFrom' <= oi.dateFrom AND '$dateTo' >= oi.dateTo)
                ) AND oi.itemId = {$this->id} AND o.id IS NOT NULL AND o.status <> 'cancelled' AND o.status <> 'closed'
        ";  

        $count = Yii::$app->db->createCommand($q)->queryScalar();
        return (int)$count;
    }

    public function countAvailable($dateFrom, $dateTo) {
        $bookedCount = $this->bookedCount($dateFrom,$dateTo);
        $totalItems = $this->stock;

        return $totalItems-$bookedCount;
    }

    public function isAvailable($dateFrom, $dateTo, $count = 1) {
        if ($this->countAvailable($dateFrom, $dateTo) >= $count) return true;
        return false;
    }

    public static function findAllAvailable($dateFrom, $dateTo) {
        $q = "
            SELECT oi.itemId, sum(count) as countBooked FROM order_item oi
            LEFT JOIN `order` o ON o.id = oi.orderId
            WHERE 
                (
                  (date('$dateFrom')  BETWEEN oi.dateFrom AND oi.dateTo) OR 
                  (date('$dateTo')  BETWEEN oi.dateFrom AND oi.dateTo) OR 
                  ('$dateFrom' <= oi.dateFrom AND '$dateTo' >= oi.dateTo)
                ) AND o.id IS NOT NULL AND o.status <> 'cancelled' AND o.status <> 'closed'
            GROUP BY oi.itemId
        ";

        return Item::find()->where("id IN 
            (SELECT item.id FROM item LEFT JOIN (
                $q 
            ) available ON available.itemId = item.id WHERE (item.stock - ifnull(countBooked,0)) > 0)");
    }

    public function upload()
    {
            // echo 'aaa';die();
        if ($this->validate()) {
            $storagePath = $this->getStoragePath();

            $saveTo = $storagePath.'/'.$this->id;
            if (!file_exists($saveTo)) mkdir($saveTo);
            
            $fullPath = $saveTo.'/original.' . $this->imageFile->extension;
            $thumbnailFullPath = $saveTo.'/thumb.' . $this->imageFile->extension;
            $detailThumbnailPath = $saveTo.'/detail.' . $this->imageFile->extension;

            $this->imageFile->saveAs($fullPath);
            
            Image::thumbnail($fullPath, 193,261)
                ->save($thumbnailFullPath, ['quality' => 80]);

            Image::thumbnail($fullPath, 304,412)
                ->save($detailThumbnailPath, ['quality' => 80]);

            $this->imagePath = \Yii::$app->urlManagerFrontend->createUrl('images/upload/'.$this->id.'/original.'.$this->imageFile->extension);
            $this->thumbnailPath = \Yii::$app->urlManagerFrontend->createUrl('images/upload/'.$this->id.'/thumb.'.$this->imageFile->extension);
            $this->detailThumbnailPath = \Yii::$app->urlManagerFrontend->createUrl('images/upload/'.$this->id.'/detail.'.$this->imageFile->extension);

            $this->save(false, array('imagePath', 'thumbnailPath', 'detailThumbnailPath'));
            return true;
        } else {
            // vd($_POST);
            vd($this->errors);
            return false;
        }
    }

    public function getStoragePath() {
        if (isset(Yii::$app->params['storagePath'])) return Yii::$params['storagePath'];

        return __DIR__.'/../../frontend/web/images/upload';
    }


    public function getImageFrontendPath() {
    }

    public function getThumbnailFrontendPath() {
    }
}
