<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace common\models;

use dektrium\user\traits\ModuleTrait;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "profile".
 *
 * @property integer $user_id
 * @property string  $name
 * @property string  $public_email
 * @property string  $gravatar_email
 * @property string  $gravatar_id
 * @property string  $location
 * @property string  $website
 * @property string  $bio
 * @property User    $user
 *
 * @author Dmitry Erofeev <dmeroff@gmail.com
 */
class Profile extends \dektrium\user\models\Profile
{
    use ModuleTrait;

    /**
     * Returns avatar url or null if avatar is not set.
     * @param  int $size
     * @return string|null
     */
    public function getAvatarUrl($size = 200)
    {
        $protocol = \Yii::$app->request->isSecureConnection ? 'https' : 'http';

        return $protocol . '://gravatar.com/avatar/' . $this->gravatar_id . '?s=' . $size;
    }

    /**
     * @return \yii\db\ActiveQueryInterface
     */
    public function getUser()
    {
        return $this->hasOne($this->module->modelMap['User'], ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            'bioString'            => ['bio', 'string'],
            'publicEmailPattern'   => ['public_email', 'email'],
            'gravatarEmailPattern' => ['gravatar_email', 'email'],
            'websiteUrl'           => ['website', 'url'],
            'nameLength'           => ['name', 'string', 'max' => 255],
            'publicEmailLength'    => ['public_email', 'string', 'max' => 255],
            'gravatarEmailLength'  => ['gravatar_email', 'string', 'max' => 255],
            'locationLength'       => ['location', 'string', 'max' => 255],
            'websiteLength'        => ['website', 'string', 'max' => 255],

            'street'               => ['street', 'string', 'max' => 255],
            'city'                 => ['city', 'string', 'max' => 255],
            'zip'                  => ['zip', 'string', 'max' => 255],

            'otherStreet'          => ['otherStreet', 'string', 'max' => 255],
            'otherCity'            => ['otherCity', 'string', 'max' => 255],
            'otherZip'             => ['otherZip', 'string', 'max' => 255],

            'ssn'                  => ['ssn', 'string', 'max' => 255],
            'idNumber'             => ['idNumber', 'string', 'max' => 255],
            'phone'                => ['phone', 'string', 'max' => 255],

            'number'                => ['number', 'string', 'max' => 255],

            // 'name'              => ['street', 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name'           => \Yii::t('user', 'Meno'),
            'public_email'   => \Yii::t('user', 'Email (public)'),
            'gravatar_email' => \Yii::t('user', 'Gravatar email'),
            'location'       => \Yii::t('user', 'Location'),
            'website'        => \Yii::t('user', 'Website'),
            'bio'            => \Yii::t('user', 'Bio'),

            'street'         => \Yii::t('user', 'Ulica'),
            'city'           => \Yii::t('user', 'Mesto'),
            'zip'            => \Yii::t('user', 'PSČ'),

            'otherStreet'    => \Yii::t('user', 'Ulica'),
            'otherCity'      => \Yii::t('user', 'Mesto'),
            'otherZip'       => \Yii::t('user', 'PSČ'),

            'ssn'            => \Yii::t('user', 'r.č.'),
            'idNumber'       => \Yii::t('user', 'č.op'),
            'phone'          => \Yii::t('user', 'telefón'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if ($this->isAttributeChanged('gravatar_email')) {
            $this->setAttribute('gravatar_id', md5(strtolower(trim($this->getAttribute('gravatar_email')))));
        }

        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%profile}}';
    }
}
