<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "item_type".
 *
 * @property integer $id
 * @property string $name
 */
class ItemHasType extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'item_has_type';
    }

  
  
}
