<?php
return [
    'adminEmail' => 'tkjhr@tkjhr.sk',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
];
