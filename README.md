TKJHR projekt
===============================

Spravene z Yii2 Advanced template

Instalacia
----------

Treba mat nainstalovany composer.
Do composera treba plugin: 

composer global require "fxp/composer-asset-plugin:~1.1.1"


Po git clone z repozitara

composer install

yii init 


Konfiguracia
------------

common/config/main-local.php, treba doplnit nazov databazy, meno, heslo

```php

<?php
return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=',
            'username' => '',
            'password' => '',
            'charset' => 'utf8',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'useFileTransport' => true,
            'viewPath' => '@common/mail',
             
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                // 'host' => '',
                // 'username' => '',
                // 'password' => '',
                // 'port' => '',
                // 'encryption' => 'tls',
        ],
    ],
    ],
];
?>
```

backend/config/main-local.php
treba doplnit frontend base url kvoli obrazkom

```php

<?php

$config = [
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'xKblBr3VkCnIAFGUad3XbfRQiGkLNVna',
        ],

        'urlManagerFrontend' => [
            'class' => 'yii\web\urlManager',
            'baseUrl' => '',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
        ],
    ],
    'modules' => [
        'user' => [
            'admins' => ['admin']
        ],
        'gii'
    ]
];

return $config;
?>
```

Migracie
--------
Doplni a aktualizuje databazu, ale po instalacii treba importnut common/init.sql.

potom uz vzdy "php yii init migrate"




