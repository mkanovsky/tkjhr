tema tak ako je kupena a stiahnuta z internetu je to kvoli referencii k dokumentacii 

KYBook
Responsive eCommerce Theme

Created: 20_October-2015
Latest update: 26-January-2016
By: Ibrahim Ibn Dawood & Akther Jabeen
Email: transvelo@support.assembla.com
Demo: http://demo2.transvelo.in/html/kybook/
Hello and thank you for purchasing this website template! KYBook is the perfect high-quality solution for those who want a beautiful eCommerce website in no time. It‘s fully responsive and will adapt itself to any mobile device. iPad, iPhone, Android, it doesn‘t matter. Your content will always looks its absolute best. This documentation guides you through the main features of this template and shows you how to use and customize them. If you have any further questions just drop us a line to transvelo@support.assembla.com