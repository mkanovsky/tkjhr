<?php

namespace backend\controllers;

use Yii;
use common\models\Order;
use common\models\OrderItem;
use common\models\Item;
use yii\data\ActiveDataProvider;
// use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use common\components\Helpers;
use dektrium\user\models\User as User;

/**
 * OrderController implements the CRUD actions for Order model.
 */
class OrderController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Order models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Order::find(),
            'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]]
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Order model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Order model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Order();

        if ($model->load(Yii::$app->request->post())) {
            $model->dateFrom = \common\components\Helpers::formatDateToDb($model->dateFrom);
            $model->dateTo = \common\components\Helpers::formatDateToDb($model->dateTo);

            $model->dateAdded = date('Y-m-d H:i:s');

            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Objednávka vytvorená');
                return $this->redirect(['order/update', 'id'=>$model->id]);
            }

        }

        $model->dateFrom = \common\components\Helpers::formatDateFromDb($model->dateFrom);
        $model->dateTo = \common\components\Helpers::formatDateFromDb($model->dateTo);

        return $this->render('create', ['model' => $model]);
    }

    /**
     * Updates an existing Order model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {

            $model->dateFrom = \common\components\Helpers::formatDateToDb($model->dateFrom);
            $model->dateTo = \common\components\Helpers::formatDateToDb($model->dateTo);
            if ($model->save()) {
                if (isset($_POST['OrderItem'])) $this->saveOrderItems($model, $_POST['OrderItem']);

                $model->updateTotalPrice();

                Yii::$app->session->setFlash('success', 'Objednávka uložená');
            }

        }

        $model->dateFrom = \common\components\Helpers::formatDateFromDb($model->dateFrom);
        $model->dateTo = \common\components\Helpers::formatDateFromDb($model->dateTo);

        return $this->render('update', ['model' => $model]);
    }

    private function saveOrderItems($order, $items) {
        $ids = [0];

        foreach ($items as $key=>$item) {
        
            $item['dateFrom'] = Helpers::formatDateToDb(trim($item['dateFrom']));
            $item['dateTo'] = Helpers::formatDateToDb(trim($item['dateTo']));

            if (is_numeric($key)) {
                $orderItem = OrderItem::findOne($item['id']);
                $orderItem->setAttributes($item);
                $orderItem->save();
                $ids[] = $orderItem->id;
            } else {
                $orderItem = new OrderItem;
                unset($item['id']);
                $orderItem->orderId = $order->id;
                $orderItem->setAttributes($item);
                $orderItem->save();
                $ids[] = $orderItem->id;
            }
        }

        $itemIds = implode(',', $ids);
        \Yii::$app->db->createCommand("DELETE FROM order_item WHERE orderId = {$order->id} AND id NOT IN ($itemIds)")->execute();

    }

    /**
     * Deletes an existing Order model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }


    public function actionItemSuggest($term) {
        
        $items = \common\models\Item::find()->where('name LIKE "%'.$term.'%"')->all();
        $r = [];

        foreach ($items as $item) {
            $tmp['id'] = $item->id;
            $tmp['label'] = $item->name;
            $tmp['price'] =$item->price;
            $tmp['typeName'] =$item->type->name;
            $r[] = $tmp;
        }
        echo json_encode( $r );
        die();

    }

    public function actionUserDetail($id) {
        $this->layout = 'clean';
        $user = User::findOne($id);
        return $this->render('_userDetail', ['user'=>$user]);
    }

    /**
     * Finds the Order model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Order the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
