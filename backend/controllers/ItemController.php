<?php

namespace backend\controllers;

use Yii;
use common\models\Item;
use common\models\ItemHasType;

use yii\data\ActiveDataProvider;
// use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use common\components\Helpers;
/**
 * ItemController implements the CRUD actions for Item model.
 */
class ItemController extends Controller
{

    var $enableCsrfValidation = false;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Item models.
     * @return mixed
     */
    public function actionIndex()
    {

        $model = new Item;

        if (isset($_GET['Item']) && $_GET['Item']['dateFrom'] && $_GET['Item']['dateTo']) {

            $model->dateFrom = Helpers::formatDateToDb($_GET['Item']['dateFrom']);
            $model->dateTo = Helpers::formatDateToDb($_GET['Item']['dateTo']);

            $dataProvider = new ActiveDataProvider([
                'query' => \common\models\Item::findAllAvailable($model->dateFrom, $model->dateTo),
                'pagination' => [
                    'pageSize' => 20,
                ],
            ]);

        } else {
            $dataProvider = new ActiveDataProvider([
                'query' => Item::find(),
            ]);
        }

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'model'=>$model
        ]);
    }

    /**
     * Displays a single Item model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Item model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Item();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            
            $this->updateTypes($model->id);  

            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
            if ($model->imageFile) $model->upload();

            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Item model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            
            $this->updateTypes($id);            

            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
            if ($model->imageFile) $model->upload();

            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }


    public function updateTypes($id) {
        $typeIds = $_POST['categories'];

        $and = "";
        if ($typeIds) {
            $typeIdsString = implode(',', $typeIds);
            $and = "AND itemTypeId NOT IN ($typeIdsString)";
        }

        $q = "
            DELETE FROM item_has_type WHERE itemId = {$id} $and
        ";
        \Yii::$app->db->createCommand($q)->execute();

        foreach ($typeIds as $tid) {
            $iht = new ItemHasType;
            $iht->itemId = $id;
            $iht->itemTypeId = $tid;
            $iht->save();
        }
    }    

    /**
     * Deletes an existing Item model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }



    /**
     * Finds the Item model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Item the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Item::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
