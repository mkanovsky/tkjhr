<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\Item */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="item-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>


    <?= $form->field($model, 'name')->textInput() ?>
    
    <?php  
        $types=\common\models\ItemType::find()->all();

        $options = [];

        foreach ($types as $type) {
            if ($type->isLeaf())
                $options[$type->id] = $type->getBreadcrumbs(3, '/', false);
        }
	
        $values = [];

        foreach ($model->getTypes()->all() as $type) {
            $values[] = $type->id;
        }
    ?>

    <?php 

    // Without model and implementing a multiple select
    echo '<label class="control-label">Kategórie</label>';
    echo Select2::widget([
        'name' => 'categories',
        'data' => $options,
        'value'=> $values,
        'options' => [
            'placeholder' => 'Select provinces ...',
            'multiple' => true
        ],
    ]);

    ?>

    <?= $form->field($model, 'stock')->textInput() ?>

    <?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'price2')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'price3')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'text')->textArea() ?>

    <?= $form->field($model, 'imageFile')->fileInput() ?>

     <div class="row">
        <?php if ($model->thumbnailPath) { ?>
            <img src="<?=$model->thumbnailPath;?>" />
        <?php } ?>
     </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Vytvoriť' : 'Uložiť', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
