<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Items';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="item-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Vytvor položku', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php 
    $form = ActiveForm::begin([
        'id' => 'search-form',
        'method'=>'get',
        'options' => ['class' => 'form-inline'],
    ]) 
    ?>

        <?= $form->field($model, 'dateFrom')->widget(\kartik\datetime\DateTimePicker::classname(), [
            'language' => 'sk',
            'options'=>[
                'class'=>'form-control',
            ],
            'pluginOptions' => [
                'minuteStep' => 30,
                'autoclose'=>true,
                'format' => 'dd.mm.yyyy hh:ii'
            ]
        ])->label('Dátum vyzdvihnutia'); ?>


        <?= $form->field($model, 'dateTo')->widget(\kartik\datetime\DateTimePicker::classname(), [
            'language' => 'sk',
            'options'=>[
                'class'=>'form-control',
            ],
            'pluginOptions' => [
                'minuteStep' => 30,
                'autoclose'=>true,
                'format' => 'dd.mm.yyyy hh:ii'
            ]
        ])->label('Dátum vrátenia'); ?>


       <button style="margin-bottom:10px" type="submit" class="btn btn-default">Search</button>

    <?php ActiveForm::end(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn'],

            'id',
            'type.name' => [
                'label'=>'Typ',
                'value'=>function($model) {
                    $v = '';
                    $types = $model->getTypes()->all();
                    if (isset($types[0])) $v = $types[0]->getBreadcrumbs(3,'/',false);

                    return $v;
                },
            ],
            'name',
            'stock',
            'price',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
