<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\tree\TreeView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Typy položiek');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="item-type-index">


    <?php 
    echo TreeView::widget([
        // single query fetch to render the tree
        'query'             => \common\models\ItemType::find()->addOrderBy('root, lft'), 
        'rootOptions' => ['label'=>'<span class="text-primary">TKJHR</span>'],
        'headingOptions'    => ['label' => 'Typy položiek'],
        'isAdmin'           => true,                       // optional (toggle to enable admin mode)
        'displayValue'      => 1,                           // initial display value
        //'softDelete'      => true,                        // normally not needed to change
        //'cacheSettings'   => ['enableCache' => true]      // normally not needed to change
    ]);
    ?>

</div>
