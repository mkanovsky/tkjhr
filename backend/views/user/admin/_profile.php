<?php

/*
 * This file is part of the Dektrium project
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/**
 * @var yii\web\View 					$this
 * @var dektrium\user\models\User 		$user
 * @var dektrium\user\models\Profile 	$profile
 */

?>

<?php $this->beginContent('@dektrium/user/views/admin/update.php', ['user' => $user]) ?>

<?php $form = ActiveForm::begin([
    'layout' => 'horizontal',
    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
    'fieldConfig' => [
        'horizontalCssClasses' => [
            'wrapper' => 'col-sm-9',
        ],
    ],
]); ?>

<?= $form->field($profile, 'name') ?>

<?= $form->field($profile, 'number') ?>

<?= $form->field($profile, 'ssn'); ?>
<?= $form->field($profile, 'idNumber'); ?>
<hr/>
Adresa
<?= $form->field($profile, 'street'); ?>
<?= $form->field($profile, 'zip'); ?>
<?= $form->field($profile, 'city'); ?>
<hr/>
Doručovacia adresa
<?= $form->field($profile, 'otherStreet'); ?>
<?= $form->field($profile, 'otherZip'); ?>
<?= $form->field($profile, 'otherCity'); ?>
<hr/>
Kontak
<?= $form->field($profile, 'phone'); ?>

<div class="form-group">
    <div class="col-lg-offset-3 col-lg-9">
        <?= Html::submitButton(Yii::t('user', 'Update'), ['class' => 'btn btn-block btn-success']) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>

<?php $this->endContent() ?>
