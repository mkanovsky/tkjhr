<?php 
use common\components\Helpers;
?>

<h3> Položky objednávky </h3>
<div style="margin:10px 0 10px 0">

<input class="input" id="orderItemName"/>
<input id="itemId" type="hidden">
<input id="itemPrice" type="hidden">
<input id="itemType" type="hidden">

<button id="addItem" class="btn btn-primary btn-sm"> Pridaj položku </button>
</div>
<table class="table table-bordered">
	<thead>
		<tr>
			<th>
				Názov
			</th>
			<th>	
				Cena
			</th>
			<th>
				Počet
			</th>

			<th>
				Od
			</th>

			<th>
				Do
			</th>

			<th>
			</th>
		</tr>
	</thead>
	
	<tbody id="orderTable">
		<?php foreach ($model->orderItems as $orderItem) { ?>
			<tr>
				<td>
					<input name="OrderItem[<?=$orderItem->id;?>][id]" value="<?=$orderItem->id;?>" type="hidden"/>
					<?=$orderItem->item->name;?>
				</td>
				
				<td>
					<input name="OrderItem[<?=$orderItem->id;?>][price]" value="<?=$orderItem->price;?>"/>
				</td>
				
				<td>
					<input name="OrderItem[<?=$orderItem->id;?>][count]" value="<?=$orderItem->count;?>"/>
				</td>
				
				<td>
					<input class="datepicker" type="text" name="OrderItem[<?=$orderItem->id;?>][dateFrom]" value="<?=Helpers::formatDateFromDb($orderItem->dateFrom);?>"/>
				</td>
				<td>
					<input class="datepicker" type="text" name="OrderItem[<?=$orderItem->id;?>][dateTo]" value="<?=Helpers::formatDateFromDb($orderItem->dateTo);?>"/>
				</td>

				<td>
					<a class="removeRow" href="#">odstrániť</a>
				</td>
			</tr>	
		<?php } ?>
	</tbody>
</table>



<!-- </div> -->