<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Orders';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Order', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,

         'rowOptions' => function ($model, $index, $widget, $grid){
                    
                switch ($model->status) {
                    case 'new' : 
                        return ['class' => ''];
                    break;
                        return ['class' => 'danger'];
                    case 'sent' : 
                        return ['class' => 'info'];
                    break;
                    case 'closed': 
                        return ['class' => 'success'];
                    break;
                    case 'cancelled' : 
                        return ['class' => 'warning'];
                    break;
                }

            },

        'columns' => [
            'id',
            'user.name' => [
                'label' => 'Zákazník',
                'value' => 'user.username'
            ],    
            'totalPrice' => [
                'label' => 'Suma',
                'value' => 'totalPrice'
            ],    
            'dateAdded' => [
                'label' => 'Dátum',
                'value' => 'dateAdded'
            ],
            'status',
            'dateFrom',
            'dateTo',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
