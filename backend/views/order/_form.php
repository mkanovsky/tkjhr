<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model common\models\Order */
/* @var $form yii\widgets\ActiveForm */

\yii\jui\JuiAsset::register($this);
?>

<?php $form = ActiveForm::begin(); ?>
<div class="order-form">

<div class="row">
    <div class="col-md-6">
        <legend> Objednávka </legend>

        <?php 
            $users = ArrayHelper::map(\dektrium\user\models\User::find()->orderBy('id desc')->all(),'id','username');
        ?>
        

        <?= $form->field($model, 'status')->dropDownList(
            \common\models\Order::getStatuses() 
        ) ?>

        <?= $form->field($model, 'dateFrom')->widget(\kartik\datetime\DateTimePicker::classname(), [
            'language' => 'sk',
            'options'=>[
                'class'=>'form-control',
            ],
            'pluginOptions' => [
                'minuteStep' => 30,
                'autoclose'=>true,
                'format' => 'dd.mm.yyyy hh:ii'
            ]
        ])->label('Dátum vyzdvihnutia'); ?>

        <?= $form->field($model, 'dateTo')->widget(\kartik\datetime\DateTimePicker::classname(), [
            'language' => 'sk',
            'options'=>[
                'class'=>'form-control',
            ],
            'pluginOptions' => [
                'minuteStep' => 30,
                'autoclose'=>true,
                'format' => 'dd.mm.yyyy hh:ii'
            ]
        ])->label('Dátum vrátenia'); ?>

        <?= $form->field($model, 'deliveryType')->dropDownList(
            \common\models\Order::getDeliveryTypes() 
        ) ?>

        <?= $form->field($model, 'deliveryPrice')->label('Cena za doručenie') ?>

        <?= $form->field($model, 'memo')->textarea()->label('Poznámka') ?>        


    </div>
    <div class="col-md-6">
        <legend> Zákazník </legend>

        <?= $form->field($model, 'userId')->dropDownList($users) ?>

        <div id="userDetail" class="well">
            <?php if (empty($model->errors) && !$model->isNewRecord && $user = $model->user) { ?>
                <address>
                  <strong><?=$user->profile->name;?></strong><br><br>

                  <strong><?=$model->street;?></strong><br>
                  <?=$model->zip;?>, <?=$model->city;?><br>
                  
                  <?php if ($model->hasOtherAddress()) { ?>
                      <br/>
                      <strong><u>Doručovacia adresa</u></strong><br/>
                      <strong><?=$user->profile->name;?></strong><br/><br/>

                      <strong><?=$model->otherStreet;?></strong><br>
                      <?=$model->otherZip;?>, <?=$model->otherCity;?><br>

                  <?php } ?>

                  <strong><?=$user->profile->phone;?></strong><br> <br/>
                  <strong><?=$user->email;?></strong><br> 

                  <strong><?=$user->profile->ssn;?></strong><br> 
                  <strong><?=$user->profile->idNumber;?></strong><br> 
                  
                </address>
            <?php } else { ?>
                <?php if ($user = $model->user) { ?>
                    <?php echo $this->context->renderPartial('_userDetail', ['user'=>$user]); ?>
                </div>
            <?php  }?>
        <?php } ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12" style="text-align:right">
        <h4>Suma : <?=$model->totalPrice;?></h4>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <hr/>
        <?php if (!$model->isNewRecord) { ?>
            <?php echo $this->context->renderPartial('_rows', ['model'=>$model]); ?>
        <?php } ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Vytvor objednávku' : 'Ulož objednávku', ['class' => 'btn btn-success']) ?>
        </div>

    </div>
    

</div>
</div>
<?php ActiveForm::end(); ?>