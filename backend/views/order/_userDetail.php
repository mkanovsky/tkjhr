<u><i>Zákazník</i></u>
<address>
  <strong><?=$user->profile->name;?></strong><br><br>
  <strong><?=$user->profile->street;?></strong><br>
  <?=$user->profile->zip;?>, <?=$user->profile->city;?><br>
  <strong><?=$user->profile->ssn;?></strong><br> 
  <strong><?=$user->profile->idNumber;?></strong><br> 
</address>

<?php if ($user->profile->otherZip || $user->profile->otherCity || $user->profile->otherStreet) { ?>
<u><i>Doručovacia adresa</i></u>
<address>
  <strong><?=$user->profile->otherStreet;?></strong><br>
  <?=$user->profile->otherZip;?>, <?=$user->profile->otherCity;?><br>
</address>
<?php } ?>
<u><i>Kontakt</i></u>
<address>
  <strong><?=$user->profile->phone;?></strong><br>
  <strong><?=$user->email;?></strong><br> 
</address>

