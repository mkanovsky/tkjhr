$(document).ready(function() {

	$('#orderItemName').autocomplete({
		source : 'item-suggest',
    	select: function(value, data){
            $('#orderItemName').val(data.item.label);
            $('#itemId').val(data.item.id);
            $('#itemPrice').val(data.item.price);
            $('#itemType').val(data.item.typeName);
            // $('#pricelistId').val(data.item.pricelistId);
            return false;
        }
	});

	$('#addItem').click(function(e) {
		e.preventDefault();

		var itemId = $('#itemId').val();
		var itemPrice = $('#itemPrice').val();
		var itemType = $('#itemType').val();
		var count = 1;
		var name = $('#orderItemName').val();
		
		var dateFrom = $('#order-datefrom').val();
		var dateTo = $('#order-dateto').val();

		if (itemId) {
			var html = "<tr>";
			var uid = uniqid();
			var idInput = '<input name="OrderItem['+uid+'][itemId]" value="'+itemId+'" type="hidden"/>';
			html += '<td>'+name+idInput+'</td>';
			
			// html += '<td>'+itemType+'</td>';
			
			var priceInput = '<input name="OrderItem['+uid+'][price]" value="'+itemPrice+'"/>';
			html += '<td>'+priceInput+'</td>';

			var countInput = '<input name="OrderItem['+uid+'][count]" value="'+count+'"/>';
			html += '<td>'+countInput+'</td>';

			var dateFromInput = '<input class="datepicker" name="OrderItem['+uid+'][dateFrom]" value="'+dateFrom+'"/>';
			html += '<td>'+dateFromInput+'</td>';
			var dateToInput = '<input class="datepicker" name="OrderItem['+uid+'][dateTo]" value="'+dateTo+'"/>';
			html += '<td>'+dateToInput+'</td>';


			html += '<td><a class="removeRow" href="#">odstrániť</a></td>';
			html += '</tr>';
			$('#orderTable').append(html);

			jQuery(".datepicker").datetimepicker({"minuteStep":30,"autoclose":true,"format":"dd.mm.yyyy hh:ii","language":"sk"});
		}
	});

	$('.removeRow').click(function(e) {
		e.preventDefault();
		$(this).closest('tr').remove();
	})

	$('#order-userid').change(function() {
		$.get('user-detail', {id:$(this).val()}, function(html) {
			$('#userDetail').html(html);
		}, 'html');
	})

	// $('.datepicker').datetimepicker($.extend({}, $.datetimepicker.regional['sk'], {"dateFormat":"dd.mm.yy"}));
	jQuery(".datepicker").datetimepicker({"minuteStep":30,"autoclose":true,"format":"dd.mm.yyyy h:ii","language":"sk"});
})

window.unique_id_counter = 0 ;
var uniqid = function(){
    var id ;
    while(true){
        window.unique_id_counter++ ;
        id = 'uids_myproject_' + window.unique_id_counter ;
        if(!document.getElementById(id)){
            /*you can remove the loop and getElementById check if you 
              are sure that noone use your prefix and ids with this 
              prefix are only generated with this function.*/
            return id ;
        }
    }
}