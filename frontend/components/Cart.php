<?php 

namespace frontend\components;

use common\components\Helpers;
use common\models\OrderItem;

class Cart {

	public static function pushItem($itemId, $dateFrom, $dateTo, $count = 1) {
		$cart = \Yii::$app->session->get('cart');
		if (!$cart) $cart = [];

		$key = $itemId.'_'.$dateFrom.'_'.$dateTo;
		
		if (isset($cart[$key])) {
			$cart[$key] += $count;
		} else {
			$cart[$key] = $count;
		}

		\Yii::$app->session->set('cart', $cart);
	}

	public static function popItem($itemId, $dateFrom, $dateTo, $count = 1) {
	
		$cart = \Yii::$app->session->get('cart');
		if (!$cart) $cart = [];

		$key = $itemId.'_'.$dateFrom.'_'.$dateTo;
		if (isset($cart[$key])) {
			if ($cart[$key] - $count <= 0) {
				unset($cart[$key]);
			} else {
				$cart[$key] -= $count;
			}
		} 

		\Yii::$app->session->set('cart', $cart);
	}

	public static function setItemCount($itemId, $dateFrom, $dateTo, $count) {
		$cart = \Yii::$app->session->get('cart');
		if (!$cart) $cart = [];

		$key = $itemId.'_'.$dateFrom.'_'.$dateTo;
		if (isset($cart[$key])) {
			$cart[$key] = $count;
		} 

		\Yii::$app->session->set('cart', $cart);
	}

	public static function removeItems($itemId, $dateFrom, $dateTo) {
		$cart = \Yii::$app->session->get('cart');
		if (!$cart) $cart = [];

		$key = $itemId.'_'.$dateFrom.'_'.$dateTo;
		if (isset($cart[$key])) {
			unset($cart[$key]);
		} 

		\Yii::$app->session->set('cart', $cart);
	}

	public static function removeByKey($key) {
		$cart = \Yii::$app->session->get('cart');
		if (!$cart) $cart = [];

		if (isset($cart[$key])) {
			unset($cart[$key]);
		} 

		\Yii::$app->session->set('cart', $cart);
	}

	public static function getContent() {
		$cart = \Yii::$app->session->get('cart');
		
		$result = [];

		$hasPs = false;
		$hasXbox = false;

		$usedPsHra = false;
		$usedPsOvladac = false;

		$usedXboxHra = false;
		$usedXboxOvladac = false;

		if ($cart) {
			foreach ($cart as $key=>$count) {

				$values = explode('_', $key);

				$itemId = $values[0];
				$dateFrom = $values[1];
				$dateTo = $values[2];

				$item = \common\models\Item::findOne($itemId);

				if ($item->isPsKonzola()) {
					$hasPs = true;
				}

				if ($item->isXboxKonzola()) {
					$hasXbox = true;
				}

				$tmp['key'] = $key;
				$tmp['itemId'] = $itemId;
				$tmp['dateFrom'] = $dateFrom;
				$tmp['dateTo'] = $dateTo;
				$tmp['item'] = $item;
				$tmp['count'] = $count;	
				// $tmp['price'] = $item->price;
				// $tmp['price2'] = $item->price2;
				
				$dateToDb = Helpers::formatDateToDb($dateTo);
				$dateFromDb = Helpers::formatDateToDb($dateFrom);

				$days = abs(Helpers::dateDiff2($dateFromDb, $dateToDb));

				$tmp['price'] = $days * $count * $item->price;

				$basePrice = $item->price;
				if ($days >= 7) $basePrice = $item->price2;
				
				$timeFrom = explode(' ',$dateFrom)[1];
				$hourFrom = explode(':', $timeFrom)[0];


				if (in_array($hourFrom, ['22','23', '01', '02', '03', '04', '05', '06', '07'])) {				
					$tmp['price'] = ($item->price3 + ($days-1)*$basePrice) * $count;
				} else {
					$tmp['price'] = $basePrice * $count * $days;
				}

				$result[] = $tmp;
			}
		}





		foreach ($result as $i=>$row) {
			if ($hasPs) {
				if (!$usedPsOvladac) {
					if ($row['item']->isPsOvladac()) {
						$result[$i]['price'] = self::calculateItemPrice($row['item'], $row['dateFrom'], $row['dateTo'], $row['count']-1);
						$usedPsOvladac = true;
					}
				}

				if (!$usedPsHra) {
					if ($row['item']->isHra()) {
						$result[$i]['price'] = self::calculateItemPrice($row['item'], $row['dateFrom'], $row['dateTo'], $row['count']-1);
						$usedPsHra = true;
					}
				}
			}

			if ($hasXbox) {
				if (!$usedXboxOvladac) {
					if ($row['item']->isXboxOvladac()) {
						$result[$i]['price'] = self::calculateItemPrice($row['item'], $row['dateFrom'], $row['dateTo'], $row['count']-1);
						$usedXboxOvladac = true;
					}
				}

				if (!$usedXboxHra) {
					if ($row['item']->isHra()) {
						$result[$i]['price'] = self::calculateItemPrice($row['item'], $row['dateFrom'], $row['dateTo'], $row['count']-1);
						$usedXboxHra = true;
					}
				}
			}
		}


		return $result;
	}

	public static function calculateItemPrice($item, $dateFrom, $dateTo, $count) {

		if (Helpers::stringContains('.', $dateFrom)) $dateFrom = Helpers::formatDateToDb($dateFrom);
		if (Helpers::stringContains('.', $dateTo)) $dateTo = Helpers::formatDateToDb($dateTo);

		$days = abs(Helpers::dateDiff2($dateFrom, $dateTo));

		$basePrice = $item->price;
		if ($days >= 7) $basePrice = $item->price2;
		
		$timeFrom = explode(' ',$dateFrom)[1];
		$hourFrom = explode(':', $timeFrom)[0];


		if (in_array($hourFrom, ['22','23', '01', '02', '03', '04', '05', '06', '07'])) {				
			$price = ($item->price3 + ($days-1)*$basePrice) * $count;
		} else {
			$price = $basePrice * $count * $days;
		}


		return $price;
	}

	public static function getDays($dateFrom, $dateTo) {
		if (Helpers::stringContains('.', $dateFrom)) $dateFrom = Helpers::formatDateToDb($dateFrom);
		if (Helpers::stringContains('.', $dateTo)) $dateTo = Helpers::formatDateToDb($dateTo);

		$days = abs(Helpers::dateDiff2($dateFrom, $dateTo));
		return $days;
	}

	public static function isEvening($date) {
		$time = explode(' ',$date)[1];
		$hour = explode(':', $time)[0];

		if (in_array($hour, ['22','23', '01', '00', '02', '03', '04', '05', '06', '07'])) {				
			return true;
		} else {
			return false;
		}
	}

	public static function getStats() {
		$cart = self::getContent();

		$sumTotal = 0;
		$countTotal = 0;

		$datesFrom = [];
		$datesTo = [];

		foreach ($cart as $c) {
			
			$object = \DateTime::createFromFormat('d.m.Y H:i', trim($c['dateFrom']));
			$time = $object->getTimestamp();
			$datesFrom[] = $time;

			$object = \DateTime::createFromFormat('d.m.Y H:i', trim($c['dateTo']));
			$time = $object->getTimestamp();
			$datesTo[] = $time;

			$countTotal += $c['count'];
			$sumTotal += $c['price'];
		}

		if (!empty($datesFrom)) $dateMin= min($datesFrom); else $dateMin = '';
		if (!empty($datesTo)) $dateMax = max($datesTo); else $dateMax = '';

		return [
			'sum'=>$sumTotal,
			'count'=>$countTotal,

			'dateFrom'=> $dateMin ? date('Y-m-d H:i:s', $dateMin) : '',
			'dateTo'=> $dateMax ? date('Y-m-d H:i:s', $dateMax) : '',
		];
	}

	public static function reset() {
		\Yii::$app->session->set('cart', []);
	}

	public static function setOrderItems($order) {
		$c = self::getContent();

		foreach ($c as $n=>$c) {
			$oi = new OrderItem;
			$oi->orderId = $order->id;
			$oi->itemId = $c['itemId'];
			$oi->dateFrom = Helpers::formatDateToDb($c['dateFrom']);
			$oi->dateTo = Helpers::formatDateToDb($c['dateTo']);
			$oi->count = $c['count'];
			$oi->price = $c['price'];
			$oi->save();
		}
	}

}

?>