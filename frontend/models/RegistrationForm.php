<?php

namespace frontend\models;

use Yii;
use \dektrium\user\models\User;

/**
 * Registration form collects user input on registration process, validates it and creates new User model.
 *
 * @author Dmitry Erofeev <dmeroff@gmail.com>
 */
class RegistrationForm extends \dektrium\user\models\RegistrationForm
{

    public $profileName;
    public $profileStreet;
    public $profileZip;
    public $profileCity;
    public $profilePhone;
    public $profileOtherStreet;
    public $profileOtherZip;
    public $profileOtherCity;

    public $password1;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        $user = $this->module->modelMap['User'];

        return [
            // username rules
            'usernameLength'   => ['username', 'string', 'min' => 3, 'max' => 255],
            'usernameTrim'     => ['username', 'filter', 'filter' => 'trim'],
            // 'usernamePattern'  => ['username', 'match', 'pattern' => $user::$usernameRegexp],
            'usernameRequired' => ['username', 'required'],
            'usernameUnique'   => [
                'username',
                'unique',
                'targetClass' => $user,
                'message' => Yii::t('user', 'This username has already been taken')
            ],
            // email rules
            'emailTrim'     => ['email', 'filter', 'filter' => 'trim'],
            'emailRequired' => ['email', 'required'],
            'emailPattern'  => ['email', 'email'],
            'emailUnique'   => [
                'email',
                'unique',
                'targetClass' => $user,
                'message' => Yii::t('user', 'This email address has already been taken')
            ],

            'password1' => ['password1', 'required'],
            // password rules
            'passwordRequired' => ['password', 'required', 'skipOnEmpty' => $this->module->enableGeneratingPassword],
            'passwordLength'   => ['password', 'string', 'min' => 6],

            [['profileStreet', 'profileZip', 'profileCity', 'profilePhone'], 'required'],
            [['profileOtherStreet', 'profileOtherZip', 'profileOtherCity'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'email'    => Yii::t('user', 'Email'),
            'username' => Yii::t('user', 'Username'),
            'password' => Yii::t('user', 'Password'),

            'profilePhone' => Yii::t('user', 'Tel. číslo'),
            'profileCity' => Yii::t('user', 'Mesto'),
            'profileZip' => Yii::t('user', 'ZIP'),
            'profileStreet' => Yii::t('user', 'Ulica'),
            'profileName' => Yii::t('user', 'Meno'),

            'profileName' => Yii::t('user', 'Meno'),

        ];
    }

    /**
     * @inheritdoc
     */
    public function formName()
    {
        return 'register-form';
    }

    /**
     * Registers a new user account. If registration was successful it will set flash message.
     *
     * @return bool
     */
    public function register()
    {   

        // $this->username = $this->profileName;

        $fullName = $_POST['register-form']['username'];

        if (!$this->validate() | $this->password != $this->password1) {
            $this->addError('password1', 'Heslá sa musia zhodovať');
            $this->addError('password', 'Heslá sa musia zhodovať');            
            return false;
        }

        if (!$this->validate()) {
            return false;
        }

        /** @var User $user */
        $user = Yii::createObject(User::className());
        $user->setScenario('register');
        $this->loadAttributes($user);

        $user->username = $this->email;

        

        if (!$user->register()) {
            $this->username = $fullName;
            return false;
        } else {
            $profile = $user->profile;

            $this->username = $this->email;
            
            $profile->number = $this->generateUniqueNumber();

            $profile->name = $fullName;
            
            $profile->zip = $this->profileZip;
            $profile->city = $this->profileCity;
            $profile->street = $this->profileStreet;

            $profile->otherZip = $this->profileOtherZip;
            $profile->otherCity = $this->profileOtherCity;
            $profile->otherStreet = $this->profileOtherStreet;

            $profile->phone = $this->profilePhone;

            $profile->save();
        }

        Yii::$app->session->setFlash(
            'info',
            Yii::t('user', 'Your account has been created and a message with further instructions has been sent to your email')
        );

        return true;
    }

    private function generateUniqueNumber() {
        $number = \common\components\Helpers::generateNumber(6);
        $r = \Yii::$app->db->createCommand("SELECT number FROM profile WHERE number='$number'")->queryScalar();
        if ($r) return $this->generateUniqueNumber();
        return $number;
    }   

    /**
     * Loads attributes to the user model. You should override this method if you are going to add new fields to the
     * registration form. You can read more in special guide.
     *
     * By default this method set all attributes of this model to the attributes of User model, so you should properly
     * configure safe attributes of your User model.
     *
     * @param User $user
     */
    protected function loadAttributes(User $user)
    {
        $user->setAttributes($this->attributes);
    }
}
