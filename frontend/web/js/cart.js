$(document).ready(function () {

	$('.addToCart').click(function (e) {
		e.preventDefault();

		var itemId = $(this).attr('item-id');

		var dateFrom = $('#dateFromInput').val();
		var dateTo = $('#dateToInput').val();

		$.post(appData.baseUrl+'/cart/add-item', {
			itemId:itemId,
			dateFrom:dateFrom,
			dateTo:dateTo
		}, function(r){
			if (r.status == 1) {
				notifyCartOk();
				refreshCartWidget();
			}
		}, 'json');

	});

	$('.removeItem').click(function (e) {
		e.preventDefault();

		var cartId = $(this).attr('cart-item-id');
		var $this = $(this);

		$.post(appData.baseUrl+'/cart/remove-item', {
			cartKey : cartId
		}, function(j) {
			if (j.status == 1) {
				refreshCartWidget();
				$this.closest('tr').remove();
			}
		}, 'json');
	})


	function notifyCartOk() {
		// $('#notifyRow').show();
		// $('#notifyRow').html('<div class="alert alert-success" role="alert"> Položka pridaná do košíka </div>');

		$.notify('Položka bola pridaná do košíka', {type:'warning'});
	}

	function refreshCartWidget() {
		$.get(appData.baseUrl+'/cart/get-stats', {}, function(j) {
			$('#itemsCount').html(j.count);
			$('#itemsSum').html(j.sum);
			$('#itemsSum1').html(j.sum);
			$('#cartSum').html(j.sum);
		}, 'json');
	}

	
	$('#dateFromInput, #dateToInput').change(function() {
		$('#datechangeform').submit();
	})


	$('#confirmForm').submit(function(e){ 
		if (!$("#optionsRadios").is(':checked')) {
			alert('Aby ste mohli pokračovať v objednávke, musíte súhlasiť s obchodnými podmienkami');
			return false;
		}
	});


	$('#registration-form').submit(function(e){ 
		if (!$("#optionsRadios").is(':checked')) {
			alert('Aby ste mohli pokračovať v objednávke, musíte súhlasiť s obchodnými podmienkami');
			return false;
		} else {
			return true;
		}
	});

})
