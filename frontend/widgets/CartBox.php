<?php 

namespace frontend\widgets;

use Yii;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use frontend\components\Cart;

class CartBox extends Widget {


	public function run() {

		$cartContent = Cart::getContent();

		return $this->render('cartBox', [
			'cartContent'=>$cartContent
        ]);
	}

}