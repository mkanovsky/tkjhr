<h4> Obsah košíka </h4>
<?php if (!empty($cartContent)) { ?>
	<table class="table">

		<?php foreach ($cartContent as $i=>$data) { ?>
			<tr>
				<td>
					<?=$data['item']->name;?>
				</td>
				<td>
					 <?=$data['dateFrom'];?>
					- <?=$data['dateTo'];?>
				</td>

				<td>
					<?=$data['count'];?> ks
				</td>

				<td>
					<?=$data['price'];?> EUR
				</td>
			</tr>
		<?php } ?>

	</table>

	<a class="btn btn-primary" href="<?=\yii\helpers\Url::to(['cart/index']);?>">do košíka</a>
<?php } else { ?>
	Košík je prázdny
<?php } ?>