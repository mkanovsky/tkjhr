<?php 

namespace frontend\widgets;

use Yii;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use frontend\components\Cart as CartComponent;

class Cart extends Widget {


    public function run() {

        $cartContent = CartComponent::getContent();

        return $this->render('cart', [
            'cartContent'=>$cartContent
        ]);
    }

}