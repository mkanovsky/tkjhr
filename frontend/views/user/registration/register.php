<?php 
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
?>

<style>
    .has-error label {
        color:#a94442;
    }

    .has-error input {
        background-color:#EDBAB9;
    }

    #agreeForm label {
        font-weight:bold;font-size:20px; color:black !important;
    }

    #agreeForm label a {
        color:black !important;
        text-decoration:underline !important;
    }

    #agreeBox:hover {
        cursor:pointer;
    }
</style>
<div class="container">
        <div class="page-header">
            <h2 class="page-title">Registrácia</h2>
            <!-- <p class="page-subtitle">DO eiusmod tempor incididunt ut labore et dolore magna aliqua</p> -->
        </div><!-- /.page-header -->
        
        <?php $form = ActiveForm::begin([
            'id'                     => 'registration-form',
            'enableAjaxValidation'   => true,
            'enableClientValidation' => false,
        ]); ?>

        <div class="page-body">


            

            <div id="accordion" class="panel-group">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a href="javascript:void(0)" class="">
                                <span class="step">1</span>Prihlasovacie údaje
                            </a>
                        </h4>
                    </div><!-- /.panel-heading -->

                    <div class="panel-body" aria-expanded="true" style="">
                        <div class="panel-body">
                        
                            <div class="form-container">
                               
                                    <div class="row field-row form-group">
                                        
                                        <?php
                                            // echo $form->field($model, 'email');
                                        ?>

                                        <div class="col-xs-12 col-sm-6 <?=$model->hasErrors('email') ? 'has-error' : '';?>">
                                            <label>
                                                Email <span class="astk">*</span> <?=$model->hasErrors('email') ? '('.$model->errors['email'][0].')' : '';?>
                                                
                                            </label>
                                            <input type="text" value="<?=$model->email;?>" name="register-form[email]" class="form-control form-control-book error">
                                        </div><!-- /.col -->

                                        <div class="col-xs-12 col-sm-3 <?=$model->hasErrors('password') ? 'has-error' : '';?>">
                                            <label>
                                                Heslo
                                                <span class="astk">*</span> <?=$model->hasErrors('password') ? '('.$model->errors['password'][0].')' : '';?>
                                            </label>
                                            <input type="password" name="register-form[password]" class="form-control form-control-book">
                                        </div><!-- /.col -->

                                        <div class="col-xs-12 col-sm-3 <?=$model->hasErrors('password1') ? 'has-error' : '';?>">
                                            <label>
                                                Zopakujte heslo
                                                <span class="astk">*</span> 
                                            </label>
                                            <input type="password" name="register-form[password1]" class="form-control form-control-book">
                                        </div><!-- /.col -->
                                    </div><!-- /.row -->

                                    <div class="row field-row form-group">
                                        <div class="col-xs-12 col-sm-6 <?=$model->hasErrors('username') ? 'has-error' : '';?>">
                                            <label>
                                                Celé meno
                                                <span class="astk">*</span> <?=$model->hasErrors('username') ? '('.$model->errors['username'][0].')' : '';?>
                                            </label>
                                            <input type="text" value="<?=$model->username;?>" name="register-form[username]" class="form-control form-control-book">
                                        </div><!-- /.col -->
                                        <div class="col-xs-12 col-sm-6 <?=$model->hasErrors('profilePhone') ? 'has-error' : '';?>">
                                            <label>
                                                Telefónne číslo
                                                <span class="astk">*</span> <?=$model->hasErrors('profilePhone') ? '('.$model->errors['profilePhone'][0].')' : '';?>
                                            </label>
                                            <input value="<?=$model->profilePhone;?>" name="register-form[profilePhone]" type="text" class="form-control form-control-book">
                                        </div><!-- /.col -->
                                    </div><!-- /.row -->
                            </div><!-- /.form-container -->
                            
                        </div><!-- /.panel-body -->
                    </div><!-- /.panel-collapse -->
                </div><!-- /.panel -->

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a href="javascript:void(0)"  aria-expanded="true">                  
                                <span class="step">2</span>Trvalé bydlisko
                            </a>                        
                        </h4>
                    </div><!-- /.panel-heading -->

                    <div class="panel-body" aria-expanded="true">
                        <div class="panel-body">
                            <div class="form-container">
                                <form role="form" class="cnt-form">
                                    <div class="row field-row form-group">
                                        <div class="col-xs-12 col-sm-4 <?=$model->hasErrors('profileStreet') ? 'has-error' : '';?>">
                                            <label>
                                                Ulica
                                                <span class="astk">*</span> <?=$model->hasErrors('profileStreet') ? '('.$model->errors['profileStreet'][0].')' : '';?>
                                            </label>
                                            <input type="text" value="<?=$model->profileStreet;?>" name="register-form[profileStreet]"  data-placeholder="street address" class="form-control form-control-book">
                                        </div><!-- /.col -->
                                        <div class="col-xs-12 col-sm-4 <?=$model->hasErrors('profileZip') ? 'has-error' : '';?>">
                                            <label> PSČ
                                                <span class="astk">*</span> <?=$model->hasErrors('profileZip') ? '('.$model->errors['profileZip'][0].')' : '';?>
                                            </label>
                                            <input type="text" value="<?=$model->profileZip;?>" name="register-form[profileZip]" data-placeholder="town" class="form-control form-control-book">
                                        </div><!-- /.col -->

                                        <div class="col-xs-12 col-sm-4 <?=$model->hasErrors('profileCity') ? 'has-error' : '';?>">
                                            <label> Mesto
                                                <span class="astk">*</span> <?=$model->hasErrors('profileCity') ? '('.$model->errors['profileCity'][0].')' : '';?>
                                            </label>
                                            <input type="text" value="<?=$model->profileCity;?>" name="register-form[profileCity]" data-placeholder="town" class="form-control form-control-book">
                                        </div><!-- /.col -->
                                    </div><!-- /.row -->

                                </form><!-- /.cnt-form -->
                            </div><!-- /.form-container -->
                        </div><!-- /.panel-body -->
                    </div><!-- /.panel-collapse -->
                </div><!-- /.panel -->

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a href="#collapseThree" data-parent="#accordion" class="collapsed" data-toggle="collapse" aria-expanded="false">
                                <span class="step">3</span>Doručovacia adresa
                            </a>
                        </h4>
                    </div><!-- /.panel-heading -->
                    <div class="panel-collapse collapse" id="collapseThree" aria-expanded="false" style="height: 0px;">
                        <div class="panel-body">
                            <div class="form-container">
                                <form role="form" class="cnt-form">
                                    <div class="row field-row form-group">
                                        <div class="col-xs-12 col-sm-4 <?=$model->hasErrors('profileOtherStreet') ? 'has-error' : '';?>">
                                            <label>
                                                Ulica
                                            </label>
                                            <input type="text" value="<?=$model->profileOtherStreet;?>" data-placeholder="street address" class="form-control form-control-book">
                                        </div><!-- /.col -->
                                        <div class="col-xs-12 col-sm-4 <?=$model->hasErrors('profileOtherZip') ? 'has-error' : '';?>">
                                            <label> PSČ</label>
                                            <input type="text" value="<?=$model->profileOtherZip;?>" data-placeholder="town" class="form-control form-control-book">
                                        </div><!-- /.col -->

                                        <div class="col-xs-12 col-sm-4 <?=$model->hasErrors('profileOtherCity') ? 'has-error' : '';?>">
                                            <label> Mesto</label>
                                            <input type="text" value="<?=$model->profileOtherCity;?>" data-placeholder="town" class="form-control form-control-book">
                                        </div><!-- /.col -->
                                    </div><!-- /.row -->

                                </form><!-- /.cnt-form -->
                            </div><!-- /.form-container -->
                        </div><!-- /.panel-body -->
                    </div><!-- /.panel-collapse -->
                </div><!-- /.panel -->

           <!-- <?php if (!empty($model->errors)) { ?>
                <div class="panel panel-default alert alert-danger">
                    <h2> Chyby vo formulári </h2>
                    <?php foreach ($model->errors as $field=>$errors) { ?>
                        <?php foreach ($errors as $error) { ?>
                            <?php if ($field != 'username') { ?>
                                <?=$error;?><br/>
                            <?php } ?>
                        <?php } ?>
                    <?php } ?>   
                </div>
            <?php } ?>-->

            </div><!-- /.panel-group -->
            
            <div class="col-md-12">
                <div class="checkout-checkbox-option pull-right" id="agreeForm">                                      
                    <input type="checkbox" value="option2" id="optionsRadios" name="optionsRadios" class="book-radio">                                        
                    <label for="optionsRadios" class="book-radio-label">
                        <span id="agreeBox" class="radio-background"><i class="icon fa fa-circle"></i></span> Súhlasím s <a href="<?=Url::to(['/site/vop']);?>">obchodnými podmienkami</a>
                    </label>
                </div>
            </div>



            <div class="col-md-12">
                <button type="submit" class="pull-right btn btn-primary btn-checkout">
                    Pokračovať
                    <i class="fa fa-chevron-right"></i>
                </button>
            </div>
        </div><!-- /.page-body -->

        <?php ActiveForm::end(); ?>
    </div>

<?php $this->registerJsFile('@web/js/cart.js',['depends' => ["frontend\assets\AppAsset"]]); ?>