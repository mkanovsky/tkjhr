<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

/**
 * @var dektrium\user\models\User
 */
?>
<?= Yii::t('user', 'Vitajte') ?>,

<?= Yii::t('user', 'Váš účet na www.tkjhr.sk je vytvorený.') ?>
<?php if ($module->enableGeneratingPassword): ?>
<?= Yii::t('user', 'We have generated a password for you') ?>:
<?= $user->password ?>
<?php endif ?>

<?php if ($token !== null): ?>
<?= Yii::t('user', 'Na dokončenie registrácie kliknite na link nižšie') ?>

<?= $token->url ?>

<?= Yii::t('user', 'Ak nemôžete klinúť, skopírujte a vložte URL do vášho prehliadača.') ?>
<?php endif ?>

<?= Yii::t('user', 'Ak ste sa neregistrovali na www.tkjhr.sk, môžete tento email ignorovať.') ?>
