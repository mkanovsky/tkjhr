<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

/**
 * @var dektrium\user\models\User   $user
 * @var dektrium\user\models\Token  $token
 */
?>
<?= Yii::t('user', 'Dobrý deň') ?>,

<?= Yii::t('user', 'Prijali sme Vašu žiadosť o zmenu hesla na www.tkjhr.sk.') ?>
    <?= Yii::t('user', 'Resetovanie Vášho hesla dokončíme, keď kliknete na link nižšie.') ?>

<?= $token->url ?>

<?= Yii::t('user', 'Ak nemôžete klinúť, skopírujte a vložte URL do vášho prehliadača.') ?>

<?= Yii::t('user', 'Ak ste sa neregistrovali na www.tkjhr.sk, môžete tento email ignorovať.') ?>
