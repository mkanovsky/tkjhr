<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

/**
 * @var dektrium\user\Module $module
 */
?>

<?php if ($module->enableFlashMessages): ?>
    
    <div class="container">
        <div class="center-block  not-found-page-content">
            <div class="page-header">
                <?php foreach (Yii::$app->session->getAllFlashes() as $type => $message): ?>
                    <?php if (in_array($type, ['success', 'danger', 'warning', 'info'])): ?>
                        <h2 class="page-title"><?= $message ?></h2>
                            
                    <?php endif ?>
                <?php endforeach ?>
                
                <p class="page-subtitle"><?=$this->title;?></p>
            </div><!-- /.page-header -->
        </div><!-- /.col -->            
    </div>
    
<?php endif ?>
