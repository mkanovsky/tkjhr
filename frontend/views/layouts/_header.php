<?php 
    use yii\helpers\Url;
    use frontend\components\Cart;

    $cartStats = Cart::getStats();    
?>

<style>
  
  #phone-navbar {
    display:none;

    background-color: #463f39;
    color:white;
    font-size:14px;
  }

  #phone-navbar a {
    color:white;
  }

  #phone-navbar a:hover {
    color:white;
    background-color: #463f39;
  }

  #phone-navbar li a {
    text-align:center;
  }

  @media (max-width: 760px) {
    #phone-navbar {
        display:block;
      }
  } 
</style>
    <!-- ============================================================= HEADER ============================================================= -->
    <header class="header">
        <!-- ============================================================= NAVBAR TOPBAR ============================================================= -->
       
        

        <nav class="navbar navbar-top-bar navbar-static-top">
            <div class="container">
                <ul class="navbar-nav nav">
                    <li><a href="<?=Url::to(['/']);?>">Domov</a></li>
                    <li><a href="<?=Url::to(['/o-nas']);?>">O nás</a></li>
                    <!-- <li><a href="<?=Url::to(['/reklamacie']);?>">Reklamácie</a></li> -->
                    <li><a href="<?=Url::to(['/vop']);?>">Obchodné podmienky</a></li>
                    <li><a href="<?=Url::to(['/kontakt']);?>">Kontakt</a></li>
                </ul><!-- /.navbar-nav -->

                <ul class="navbar-nav nav navbar-right">

                    <?php if (!\Yii::$app->user->isGuest) { ?>
                        <li><a href="#"><?=\Yii::$app->user->identity->email;?> (<?=\Yii::$app->user->identity->profile->number;?>)</a></li>
                        <li><a href="<?=Url::to(['/site/logout']);?>">(odhlásiť)</a></li>
                    <?php } else { ?>
                        <li><a href="<?=Url::to(['/user/login']);?>">Prihlásenie</a></li>
                    <?php } ?>

                    <li><a href="<?=Url::to(['/cart/index']);?>">Košík</a></li>
                    <li class="active"><a href="<?=Url::to(['/cart/index']);?>">Celkom : <span id="itemsSum"><?=$cartStats['sum'];?></span> &euro;</a></li>
                
                </ul><!-- /.navbar-nav -->
            </div><!-- /.container-->
        </nav><!-- /.navbar-->
        <!-- ============================================================= NAVBAR TOPBAR : END ============================================================= -->    
        <div class="yamm navbar navbar-default navbar-default-book animate-dropdown" role="navigation">
        <div class="container">
            <!-- ============================================================= NAVBAR PRIMARY ============================================================= -->

            <div class="header-mast">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#KYbook-navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <a class="navbar-brand" href="<?=Url::to(['/']);?>" style="margin:0">
                        <img width="230" src="<?=Url::to(['/images/logo_transparent.png']);?>" alt="tkjhr.sk" title="TKJHR"/>
                    </a>

                </div><!-- /.navbar-header -->
                <div class="collapse navbar-collapse" id="KYbook-navbar">
                    <ul class="nav navbar-nav navbar-right">

                        <li class="hidden-sm<?=isset($this->context->rootCId) && $this->context->rootCId ==1 ? ' active' : '';?>"><a href="<?=Url::to(['site/playstation']);?>">Playstation 4</a></li>
                        <li  class="hidden-sm<?=isset($this->context->rootCId) && $this->context->rootCId ==2 ? ' active' : '';?>"><a href="<?=Url::to(['site/xbox']);?>">X BOX</a></li>
                    </ul><!-- /.nav -->
                </div><!-- /.collapse -->
                <a href="<?=Url::to(['/cart/index']);?>" class="navbar-btn btn btn-cart"><img src="<?=Url::to(['/']);?>images/shopping-cart.png" alt="" /><span id="itemsCount" class="badge-cart-items-count"><?=$cartStats['count'];?></span></a>
            </div><!-- /.header-mast -->
            <!-- ============================================================= NAVBAR PRIMARY : END ============================================================= -->           
        </div><!-- /.container -->
    </div><!-- /.yamm -->

    <div id="phone-navbar" class="navbar">
        <ul class="navbar-nav nav navbar-right">
            <?php if (!\Yii::$app->user->isGuest) { ?>
                <li><a href="#"><?=\Yii::$app->user->identity->email;?> (<?=\Yii::$app->user->identity->profile->number;?>)</a></li>
            <?php } else { ?>
                <li><a href="<?=Url::to(['/user/login']);?>">Prihlásenie</a></li>
            <?php } ?>
            <hr style="margin:5px 0 5px 0"/>
            <li><a href="<?=Url::to(['/cart/index']);?>">Košík</a></li>
            <li class="active"><a href="<?=Url::to(['/cart/index']);?>">Celkom : <span id="itemsSum1"><?=$cartStats['sum'];?></span> &euro;</a></li>
            <hr style="margin:5px 0 5px 0"/>
            <li><a href="<?=Url::to(['/vop']);?>">Obchodné podmienky</a></li>
        </ul>
    </div>
</header><!-- /.header -->

<!-- ============================================================= HEADER : END ============================================================= -->
