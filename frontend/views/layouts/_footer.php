<?php 
    use yii\helpers\Url;
?>
<!-- ============================================================= FOOTER ============================================================= -->
<div class="container">
<div class="sub-footer-widget">
    <div class="row">

        <div class="col-md-6 hidden-sm col-xs-12">
            <!-- ============================================================= FOOTER BOOK TIPS ============================================================= -->
            <div class="module">
                <div class="module-heading">
                    <h2 class="module-title">Stačí si vybrať</h2>
                    <!-- <p class="module-subtitle">Stačí si vybrať z našej ponuky herného sortimentu a zábava sa môže začať! Ktorýkoľvek deň aj v nočných hodinách – buď ti to dovezieme, alebo môžeš prísť vyzdvihnúť osobne na Obchodnej 39. Pre zápasy a turnaje vo FIFE, NHL či UFC – alebo prechádzanie príbehu CALL OF DUTY odteraz čas nie je problém. Taká je hra.</p> -->
                </div><!-- /.module-heading -->

                <div class="module-body subfooter-content">
                    <p>
                        Stačí si vybrať z našej ponuky herného sortimentu a zábava sa môže začať! Ktorýkoľvek deň aj v nočných hodinách – buď ti to dovezieme, alebo môžeš prísť vyzdvihnúť osobne na Obchodnej 39. Pre zápasy a turnaje vo FIFE, NHL či UFC – alebo prechádzanie príbehu CALL OF DUTY odteraz čas nie je problém. Taká je hra.
                    </p>
                </div><!-- /.module-body -->
            </div><!-- /.module -->
            <!-- ============================================================= FOOTER BOOK TIPS : END ============================================================= -->

        </div><!-- /.col -->

        <div class="col-md-6 hidden-sm col-xs-12">
            <!-- ============================================================= FOOTER BOOK TIPS ============================================================= -->
            <div class="module">
                <div class="module-heading">
                    <h2 class="module-title">
                            
                     <ul id="tooltip" class="nav nav-pills social-media-list">
                        <li style="background-color:#587aca;"><a href="https://www.facebook.com/takajehra/" target="_blank"  data-placement="bottom" title="" data-toggle="tooltip" data-original-title="Facebook"><i class="icon fa fa-facebook"></i></a></li>
                        <li style="background-color:#e95950;"><a href="http://instagram.com/transvelo/" title="" data-placement="bottom" data-toggle="tooltip" data-original-title="Instagram"><i class="icon fa fa-instagram"></i></a></li>
                        <li style="background-color:#fffc00;"><a href="https://www.snapchat.com/add/tkjhr" target="_blank" title="" style="color:grey" data-placement="bottom" data-toggle="tooltip" data-original-title="Snapchat"><i class="icon fa fa-snapchat"></i></a></li>
                    </ul><!-- /.nav -->

                    </h2>
                </div><!-- /.module-heading -->

                <div class="module-body subfooter-content" style="margin-top:42px">
                    <p>
                        Nájdeš nás aj na sociálnych sietiach. Novinky z našej ponuky, akcie a zľavy, atmosféra v Games zone – to všetko na našom <a href="https://www.facebook.com/takajehra/" titke="Facebook">Facebooku</a>, <a href="#">Instagrame</a> alebo <a href="#">Snapchate</a>.
                    </p>
                </div><!-- /.module-body -->
            </div><!-- /.module -->
            <!-- ============================================================= FOOTER BOOK TIPS : END ============================================================= -->

        </div><!-- /.col -->
    </div><!-- /.row -->
</div>
</div>
<footer class="footer">
    <div class="main-footer">
        <div class="container">
            <!-- ============================================================= FOOTER NAVBAR ============================================================= -->
            <div class="navbar-footer navbar-static-bottom clearfix">
                <p class="navbar-text">&copy; 2016 <span class="navbar-inner-text">TKJHR.sk</span></p>
                <ul id="example" class="navbar-nav nav ">
                    <li><a href="<?=Url::to(['/']);?>">Domov</a></li>
                    <li><a href="<?=Url::to(['/o-nas']);?>">O nás</a></li>
                    <!-- <li><a href="<?=Url::to(['/reklamacie']);?>">Reklamácie</a></li> -->
                    <li><a href="<?=Url::to(['/vop']);?>">Obchodné podmienky</a></li>
                    <li><a href="<?=Url::to(['/kontakt']);?>">KONTAKT</a></li>
                </ul><!-- /.navbar-nav -->
            </div><!-- /.navbar -->
            <!-- ============================================================= FOOTER NAVBAR : END ============================================================= -->            
        </div><!-- /.container -->
    </div><!-- /.main-footer -->
</footer><!-- /.footer -->

<!-- ============================================================= FOOTER : END ============================================================= --> 