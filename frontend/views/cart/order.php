<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model common\models\Order */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(); ?>


<?= $form->field($order, 'deliveryType')->dropDownList(
    \common\models\Order::getDeliveryTypes() 
) ?>

<hr/>



<div class="form-group">
</div>

<?php ActiveForm::end(); ?>