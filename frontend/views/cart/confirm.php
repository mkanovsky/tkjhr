<?php 
use yii\helpers\Url;
use frontend\components\Cart;
use common\components\Helpers;

$stats = Cart::getStats();
$cartContent = Cart::getContent();
$user = \Yii::$app->user->identity;

$dt = Helpers::formatDateFromDb($stats['dateFrom']);
$dt = explode(' ', $dt);
?>

<style>
	#agreeForm label {
		font-weight:bold;font-size:20px; color:black !important;
	}

	#agreeForm label a {
		color:black !important;
		text-decoration:underline !important;
	}

	#agreeBox:hover {
		cursor:pointer;
	}
</style>

<form id="confirmForm" method="post" action="<?=Url::to(['cart/submit']);?>">
	<input type="hidden" name="Order[deliveryPriceId]" value="<?=$order->deliveryPriceId;?>"/>
	<input type="hidden" name="Order[street]" value="<?=$order->street;?>"/>
	<input type="hidden" name="Order[zip]" value="<?=$order->zip;?>"/>
	<input type="hidden" name="Order[city]" value="<?=$order->city;?>"/>
	<input type="hidden" name="Order[otherStreet]" value="<?=$order->otherStreet;?>"/>
	<input type="hidden" name="Order[otherZip]" value="<?=$order->otherZip;?>"/>
	<input type="hidden" name="Order[otherCity]" value="<?=$order->otherCity;?>"/>
	<input type="hidden" name="Order[deliveryType]" value="<?=$order->deliveryType;?>"/>
	<input type="hidden" name="Order[memo]" value="<?=$order->memo;?>"/>
	<input type="hidden" name="Order[status]" value="<?=$order->status;?>"/>
	<input type="hidden" name="Order[userId]" value="<?=$order->userId;?>"/>
	<input type="hidden" name="Order[deliveryPrice]" value="<?=$order->deliveryPrice;?>"/>
	<input type="hidden" name="Order[dateFrom]" value="<?=$order->dateFrom;?>"/>
	<input type="hidden" name="Order[dateTo]" value="<?=$order->dateTo;?>"/>
	<input type="hidden" name="Order[dateAdded]" value="<?=$order->dateAdded;?>"/>

<div class="container">
    <div class="page-header">
        <h2 class="page-title">Potvrdenie objednávky</h2>
        <!-- <p class="page-subtitle">DO eiusmod tempor incididunt ut labore et dolore magna aliqua</p> -->
    </div><!-- /.page-header -->

    <div class="page-body">

    	<table class="table table-cart cart-detail">
    		<thead>
	            <tr>
	                <th width="20%" class="dark">Názov</th>
	                <th width="40%"> Čas</th>
	                <th width="15%" class="dark text-center">Počet</th>
	                <th width="20%">Cena</th>
	            </tr>
	        </thead>
    	
    		<tbody class="cart-body-content">
        		<?php foreach ($cartContent as $i=>$data) { ?>
					<tr>
						<td>
                            <div class="media">
                                <div class="media-body">
                                    <h3 class="media-heading"><?=$data['item']->name;?></h3>
                                </div>
                            </div>
                        </td>

						<td>
							 od : <?=$data['dateFrom'];?> <br/>
							 do : <?=$data['dateTo'];?>
						</td>

						<td>
							<?=$data['count'];?> ks
						</td>

						<td>
							<?=Helpers::cuteNumber($data['price']);?> EUR
						</td>

					</tr>
				<?php } ?>

					<tr>
						<td>
                            Doprava - <?php if ($order->deliveryType == 'onplace') echo 'osobne'; ?>
                           <?php if ($order->deliveryType == 'courier') echo 'kuríer - '.\common\models\DeliveryPrice::findOne($order->deliveryPriceId)->name; ?>
                        </td>

						<td>
							<?=Helpers::formatDateFromDb($stats['dateFrom']);?>
						</td>

						<td>
							
						</td>

						<td>
							<?php 
								if ($order->deliveryType == 'courier') echo $order->deliveryPrice;
								if ($order->deliveryType == 'onplace') echo '0,00';
							?> EUR
						</td>

					</tr>
			</tbody>

    	</table>


    	<hr/>


    	<h3> Moje údaje </h3>

    	<address>
		  <strong> <?=$user->profile->name;?> </strong><br>
		  <?=$order->street;?><br>
		  <?=$order->zip;?> <?=$order->city;?><br>
		  <abbr title="Phone">P:</abbr> <?=$user->profile->phone;?>
		</address>

		<address>
		  <?=$user->email;?></address>
		</address>

		<hr/>

		<?php if ($order->deliveryType == 'onplace') { ?>
			<address>
				Objednávku si vyzdvihnem osobne na Obchodnej ulici č. 39 v Bratislave dňa <?=$dt[0];?> o <?=$dt[1];?>
			</address>
		<?php } ?>

		<?php if ($order->deliveryType == 'courier') { ?>
			<address>
				Objednávku mi doručí kuriér <?=$dt[0];?> o <?=$dt[1];?> na nasledujúcej adrese: <br/> <br/>

				<?php if ($order->hasOtherAddress()) { ?>
					<address>
					  <strong> <?=$user->profile->name;?> </strong><br>
					  <?=$order->otherStreet;?><br>
					  <?=$order->otherZip;?> <?=$order->otherCity;?><br>
					  <abbr title="Phone">P:</abbr> <?=$user->profile->phone;?>
					</address>
				<?php } else { ?>
					<address>
					  <strong> <?=$user->profile->name;?> </strong><br>
					  <?=$order->street;?><br>
					  <?=$order->zip;?> <?=$order->city;?><br>
					  <abbr title="Phone">P:</abbr> <?=$user->profile->phone;?>
					</address>
				<?php } ?> 
			</address>
		<?php } ?>


		<hr/>

		<address>		
			<?php 
				$sum = $stats['sum'];
				if ($order->deliveryType == 'courier') $sum+=$order->deliveryPrice;
			?>
			Celková cena objednávky je <?=Helpers::cuteNumber($sum);?> EUR, ktorú uhradím v hotovosti. Pri preberaní objednávky si <u>pripravím občiansky preukaz</u>.
		</address>

		<div class="col-md-12">
			<div class="checkout-checkbox-option pull-right" id="agreeForm">                                      
                <input type="checkbox" value="option2" id="optionsRadios" name="optionsRadios" class="book-radio">                                        
                <label for="optionsRadios" class="book-radio-label">
                    <span id="agreeBox" class="radio-background"><i class="icon fa fa-circle"></i></span> Súhlasím s <a href="<?=Url::to(['/site/vop']);?>">obchodnými podmienkami</a>
                </label>
            </div>
		</div>

	  	<div class="col-md-12">
            <button type="submit" id="checkoutBtn" class="pull-right btn btn-primary btn-checkout">
                Odoslať objednávku
                <i class="fa fa-chevron-right"></i>
            </button>
        </div>
    </div>
</div>

</form>
<?php $this->registerJsFile('@web/js/cart.js',['depends' => ["frontend\assets\AppAsset"]]); ?>