<?php 
use yii\helpers\Url;
?>
<div class="container">
    <div class="center-block  not-found-page-content">
        <div class="page-header">
            
            <h2 class="page-title">Nie ste prihlásený</h2>
            
            <p class="page-subtitle">Ak chcete pokračovať v objednávke, musíte sa <a href="<?=Url::to(['/user/login']);?>">prihlásiť</a> alebo <a href="<?=Url::to(['/user/register']);?>">registrovať</a>.</p>
        </div><!-- /.page-header -->
    </div><!-- /.col -->            
</div>