<?php 
use yii\helpers\Url;
use frontend\components\Cart;

$stats = Cart::getStats();

$this->title = 'Košík'.' | Požičovňa hier | TKJHR.sk';

// vd($cartContent);
// Cart::reset();
?>
        <div class="cart page">
            <div class="container">
                <div class="page-header">
                    <h2 class="page-title">Košík</h2>
                    <p class="page-subtitle">K sume vašej objednávky, sa môže pripočítať cena za doručenie v ďaľšom kroku.</p>
                </div><!-- /.page-header -->

                <?php if (empty($cartContent)) { ?>
                         <div style="text-align:center; margin:50px" class="page-body">
                            <span style="text-transform: uppercase; font-size:32px; font-weight:bold"> Váš košík je prázdny </span>
                         </div>
                    <?php } else { ?>
                        <div class="page-body">
                            <div class="order-detail table-responsive">
                                


                                    <table class="table table-cart cart-detail ">
                                		 
                                        <thead>
                                            <tr>
                                                <th width="20%" class="dark">Názov</th>
                                                <th width="40%"> Čas</th>
                                                <th width="15%" class="dark text-center">Počet</th>
                                                <th width="20%">Cena</th>
                                                <th width="10%" class="dark">&nbsp;</th>
                                            </tr>
                                        </thead>
            							 
                                        <tbody class="cart-body-content">
            		                		<?php foreach ($cartContent as $i=>$data) { ?>
            									<tr>
            										<td>
            	                                        <div class="media">
            	                                            <div class="media-body">
            	                                                <h3 class="media-heading"><a href="<?=Url::to(['site/detail', 'id'=>$data['itemId']]);?>"><?=$data['item']->name;?></a></h3>
            	                                            </div>
            	                                        </div>
            	                                    </td>

            										<td>
            											 od : <?=$data['dateFrom'];?> <br/>
            											 do : <?=$data['dateTo'];?>
            										</td>

            										<td>
            											<input style="width:60px" type="text" value="<?=$data['count'];?>"/> ks
            										</td>

            										<td>
            											<?=$data['price'];?> EUR
            										</td>

            										<td>
            											<button class="btn btn-primary-light btn-flat removeItem" cart-item-id="<?=$data['key'];?>" type="button">X</button>
            										</td>

            									</tr>
            								<?php } ?>
            							</tbody>
            						</table>
                            </div><!-- /.order-detail -->
                            <div class="row">

                                <div class="col-xs-12 col-sm-6 center-sm pull-right">
                                    <div class="table-responsive">
                                        <table class="table table-cart">
                                            <tfoot>
                                                <tr>
                                                	<td>&nbsp;</td>
                                                    <td><i class="icon-chevron fa fa-chevron-right"></i>&nbsp;Suma:</td>
                                                    <td id="cartSum"><?=$stats['sum'];?> &euro;</td>
                                                </tr>
                                            </tfoot><!-- /tfoot -->
                                        </table><!-- /table -->
                                    </div><!-- /table-responsive -->
                                </div><!-- /.col -->
                                <div class="col-md-12">
                                        <a href="<?=Url::to(['cart/checkout']);?>" class="pull-right btn btn-primary btn-checkout">Pokračovat v objednávke &nbsp;<i class="fa fa-chevron-right"></i></a>
                                </div>

                            </div><!-- /.row -->
                        </div><!-- /.page-body -->
                <?php } ?> 
            </div><!-- /.container -->
        </div><!-- /.cart page -->            <a class="scrollup hidden-xs hidden-sm" href="#" style="display: inline;"><img src="assets/images/top-scroll.png" alt=""></a>

<?php $this->registerJsFile('@web/js/cart.js',['depends' => ["frontend\assets\AppAsset"]]); ?>
