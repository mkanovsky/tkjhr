<?php 
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

use frontend\components\Cart;
use common\components\Helpers;

$this->title = 'Košík'.' | Požičovňa hier | TKJHR.sk';

$stats = Cart::getStats();

$dt = Helpers::formatDateFromDb($stats['dateFrom']);
$dt = explode(' ', $dt);

$evening = Cart::isEvening($stats['dateFrom']);

if ($evening) $order->deliveryType = 'courier'; else $order->deliveryType = 'onplace';

$colsize = $evening ? 3 : 4;

?>
<style>
    .has-error label {
        color:#a94442;
    }

    .has-error input {
        background-color:#EDBAB9;
    }

    .has-error select {
        background-color:#EDBAB9;
    }

    textarea {
        width:100%;
        resize: none; 
    }

</style>
<div class="container">
        <div class="page-header">
            <h2 class="page-title">Objednávka</h2>
            <!-- <p class="page-subtitle">DO eiusmod tempor incididunt ut labore et dolore magna aliqua</p> -->
        </div><!-- /.page-header -->

        <form method="post" id="checkoutForm">
        <div class="page-body">
            <div id="accordion" class="panel-group">
                

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a href="javascript:void(0)"  aria-expanded="true">                  
                                <span class="step">1</span>Trvalé bydlisko
                            </a>                        
                        </h4>
                    </div><!-- /.panel-heading -->

                    <div class="panel-body" aria-expanded="true">
                        <div class="form-container">
                            <div class="row field-row form-group">

                                <?php if ($evening) { ?>
                                    <div class="col-xs-12 col-sm-3 <?=$order->hasErrors('deliveryPriceId') ? 'has-error' : '';?>">
                                        <label>
                                            Oblasť
                                            <span class="astk">*</span>
                                        </label>
                                        <select name="Order[deliveryPriceId]" class="form-control form-control-book error">
                                            <option value="">
                                                Vyber
                                            </option>

                                            <?php foreach (\common\models\DeliveryPrice::find()->orderBy('name')->all() as $d) { ?>    
                                            <option value="<?=$d->id;?>">
                                                <?=$d->name;?> (doprava <?=$d->amount;?> &euro;) 
                                            </option>
                                            <?php } ?>
                                        </select>
                                    </div><!-- /.col -->
                                <?php } ?>

                                <div class="col-xs-12 col-sm-<?=$colsize;?> <?=$order->hasErrors('street') ? 'has-error' : '';?>">
                                    <label>
                                        Ulica
                                        <span class="astk">*</span>
                                    </label>
                                    <input type="text" value="<?=$order->street;?>" name="Order[street]" class="form-control form-control-book error">
                                </div><!-- /.col -->

                                <div class="col-xs-12 col-sm-<?=$colsize;?> <?=$order->hasErrors('zip') ? 'has-error' : '';?>">
                                    <label>
                                        PSČ
                                        <span class="astk">*</span>
                                    </label>
                                    <input type="text" value="<?=$order->zip;?>" name="Order[zip]" class="form-control form-control-book error">
                                </div><!-- /.col -->

                                <div class="col-xs-12 col-sm-<?=$colsize;?> <?=$order->hasErrors('city') ? 'has-error' : '';?>">
                                    <label>
                                        Mesto
                                        <span class="astk">*</span>
                                    </label>
                                    <input type="text" value="<?=$order->city;?>" name="Order[city]" class="form-control form-control-book error">
                                </div><!-- /.col -->
                            </div><!-- /.row -->
                        </div><!-- /.form-container -->
                    </div><!-- /.panel-collapse -->
                </div><!-- /.panel -->

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a <?=$otherAddress ? 'href="#" aria-expanded="true"' : 'href="#collapseThree" data-parent="#accordion" class="collapsed" data-toggle="collapse" aria-expanded="false"';?>>
                                <span class="step">3</span>Doručovacia adresa
                            </a>
                        </h4>
                    </div><!-- /.panel-heading -->
                    <div class="<?=$otherAddress ? 'panel-body' : 'panel-collapse collapse';?>" id="collapseThree" <?=$otherAddress ? 'aria-expanded="true"' : 'aria-expanded="false" style="height: 0px;"';?>>
                            <div class="form-container">
                                <div class="row field-row form-group">
                                        
                                    <?php if ($evening) { ?>
                                        <div class="col-xs-12 col-sm-<?=$colsize;?> <?=$order->hasErrors('otherDeliveryPriceId') ? 'has-error' : '';?>">
                                            <label>
                                                Oblasť
                                                <span class="astk">*</span>
                                            </label>
                                            <select name="Order[otherDeliveryPriceId]" class="form-control form-control-book error">

                                                <option value="">
                                                    Vyber
                                                </option>

                                                <?php foreach (\common\models\DeliveryPrice::find()->orderBy('name')->all() as $d) { ?>   
                                                <option value="<?=$d->id;?>">
                                                    <?=$d->name;?> (doprava <?=$d->amount;?> &euro;) 
                                                </option>
                                                <?php } ?>
                                            </select>
                                        </div><!-- /.col -->
                                    <?php } ?>

                                    <div class="col-xs-12 col-sm-<?=$colsize;?> <?=$order->hasErrors('otherStreet') ? 'has-error' : '';?>">
                                        <label>
                                            Ulica
                                        </label>
                                        <input type="text" value="<?=$order->otherStreet;?>" name="Order[otherStreet]" class="form-control form-control-book error">
                                    </div><!-- /.col -->
                                    
                                    <div class="col-xs-12 col-sm-<?=$colsize;?> <?=$order->hasErrors('otherZip') ? 'has-error' : '';?>">
                                        <label>
                                            PSČ
                                        </label>
                                        <input type="text" value="<?=$order->otherZip;?>" name="Order[otherZip]" class="form-control form-control-book error">
                                    </div><!-- /.col -->

                                    <div class="col-xs-12 col-sm-<?=$colsize;?> <?=$order->hasErrors('otherCity') ? 'has-error' : '';?>">
                                        <label>
                                            Mesto
                                        </label>
                                        <input type="text" value="<?=$order->otherCity;?>" name="Order[otherCity]" class="form-control form-control-book error">
                                    </div><!-- /.col -->
                                </div><!-- /.row -->

                            </div><!-- /.form-container -->
                    </div><!-- /.panel-collapse -->
                </div><!-- /.panel -->

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a href="javascript:void(0)" data-parent="#accordion" aria-expanded="true">
                                <span class="step">4</span>Spôsob doručenia
                            </a>
                        </h4>
                    </div><!-- /.panel-heading -->

                    <div class="panel-body" id="" aria-expanded="true">
                        <div class="panel-body">
                            <div class="form-container <?=$order->hasErrors('deliveryType') ? 'has-error' : '';?>">
                                    <div class="radio">
                                        <label>
                                            <input <?php if ($evening) echo 'disabled="disabled"';?> type="radio" value="onplace" name="Order[deliveryType]" <?=$order->deliveryType == 'onplace' ? 'checked="checked"' : '';?>>
                                                Prevzať osobne
                                            <br>
                                           Tovar si prevezmem dňa <?=$dt[0];?> o <?=$dt[1];?> na Obchodnej 33, Bratislava. <strong> Prevziať osobne je možné iba v rámci otváracích hodín (10:00 - 22:00) </strong>
                                        </label>
                                    </div><!-- /.radio -->
                                    <div class="radio">
                                        <label>
                                            <input<?php if (!$evening) echo 'disabled="disabled"';?>  type="radio" value="courier" name="Order[deliveryType]" <?=$order->deliveryType == 'courier' ? 'checked="checked"' : '';?>>
                                                Doručenie kuriérom
                                            <br>
                                           Tovar dovezie kuriér <?=$dt[0];?> o <?=$dt[1];?> na mojej doručovacej adrese. <strong> Doručenie kuriérom je možné iba v nočných hodinách (22:00 - 8:00) </strong>
                                        </label>
                                    </div><!-- /.radio -->
                            </div><!-- /.form-container-->
                        </div><!-- /.panel-body -->
                    </div><!-- /.panel-collapse -->
                </div><!-- /.panel -->

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a href="javascript:void(0)" aria-expanded="true">
                                <span class="step">5</span>Platba
                            </a>
                        </h4>
                    </div><!-- /.panel-heading -->
                    <div class="panel-body" id="" aria-expanded="true">
                        <div class="panel-body">
                            <div class="form-container">
                                <div class="radio">
                                    <label>
                                        Momentálne môžete platiť iba v hotovosti pri preberaní objednávky. Nezabudnite si občiansky preukaz.
                                    </label>
                                </div><!-- /.radio -->
                            </div><!-- /.form-container -->
                        </div><!-- /.panel-body -->
                    </div><!-- /.panel-collapse -->
                </div><!-- /.panel -->


                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a href="javascript:void(0)" aria-expanded="true">
                                <span class="step">6</span>Poznámka
                            </a>
                        </h4>
                    </div><!-- /.panel-heading -->
                    <div class="panel-body" id="" aria-expanded="true">
                        <div class="panel-body">
                            <div class="form-container">
                                <div class="row field-row form-group">
                                    <div class="col-xs-12 col-sm-12 <?=$order->hasErrors('otherStreet') ? 'has-error' : '';?>">
                                        <label>
                                        </label>
                                        
                                        <textarea placeholder="Poschodie, bránka zozadu :) ..." name="Order[memo]" class="form-control form-control-book"></textarea>
                                    </div><!-- /.col -->
                                </div><!-- /.row -->
                            </div><!-- /.form-container -->
                        </div><!-- /.panel-body -->
                    </div><!-- /.panel-collapse -->
                </div><!-- /.panel -->
            </div><!-- /.panel-group -->

            <div class="col-md-12">
                <button type="submit" id="checkoutBtn" class="pull-right btn btn-primary btn-checkout">
                    Pokračovať
                    <i class="fa fa-chevron-right"></i>
                </button>
            </div>


        </div><!-- /.page-body -->
        </form>
    </div>


<?php $this->registerJsFile('@web/js/cart.js',['depends' => ["frontend\assets\AppAsset"]]); ?>