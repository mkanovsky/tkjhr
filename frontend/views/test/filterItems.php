<?php 
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
?>

<div id="notifyRow">
</div>

<div class="row">
<div class="col-md-9">
<form method="get">

	<?php 

	// dal by sa pouzit aj daky datetime picker s presnostami na hodiny, zavisi od grafickeho navrhu

	echo DatePicker::widget([
	    'name'  => 'dateFrom',
    	'value'  => $dateFrom,
	    'language' => 'sk',
	    'options' => ['id'=>'dateFromInput'],
	    'dateFormat' => 'dd.MM.yyyy',
	]);
	?>

	<?php 
	echo DatePicker::widget([
	    'name'  => 'dateTo',
    	'value'  => $dateTo,
    	'options' => ['id'=>'dateToInput'],
	    'language' => 'sk',
	    'dateFormat' => 'dd.MM.yyyy',
	]);
	?>

	<input type="submit"/>

</form>

<table class="table"/>
	<thead>
		<tr>
			<th>
				id
			</th>
			<th>
				typ
			</th>
			<th>
				názov
			</th>
			<th>
				pocet dosupnych kusov
			</th>
			<th>
				obrazok
			</th>
			<th>
				cena za 24h
			</th>
			<th>
				do kosika
			</th>
		</tr>
	</thead>

	<tbody>
		<?php foreach ($itemsProvider->getModels() as $n=>$item) { ?>
			<tr>
				<td>
					<?=$item->id;?>
				</td>

				<td>
					<?=$item->type->name;?>
				</td>

				<td>
					<?=$item->name;?>
				</td>

				<td>
					<?=$item->countAvailable($dateFrom, $dateTo);?>
				</td>

				<td>
					<?php if ($item->thumbnailPath) { ?>
						<img src="<?=$item->thumbnailPath;?>"/>
					<?php } else { ?>
						obrazok nie je k dispozicii
					<?php } ?>
				</td>

				<td>
					<?=$item->price;?>
				</td>

				<td>
					<a class="addToCart" item-id="<?=$item->id;?>" href="#">do kosika</a>
				</td>
			</tr>
		<?php } ?>
	</tbody>
</table>

<?php 
echo \yii\widgets\LinkPager::widget([
    'pagination'=>$itemsProvider->pagination,
]);
?>

</div>

<div class="col-md-3">
	<div id="cartBox">
		<?php echo \frontend\widgets\CartBox::widget([]); ?>
	</div>
</div>

</div>

<?php $this->registerJsFile('@web/js/cart.js',['depends' => ["frontend\assets\AppAsset"]]); ?>