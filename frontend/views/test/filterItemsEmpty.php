<?php 
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
?>

<div id="notifyRow">
</div>

<div class="row">
<div class="col-md-9">
<form method="get">

	<?php 

	// dal by sa pouzit aj daky datetime picker s presnostami na hodiny, zavisi od grafickeho navrhu

	echo DatePicker::widget([
	    'name'  => 'dateFrom',
    	'value'  => '',
	    'language' => 'sk',
	    'options' => ['id'=>'dateFromInput'],
	    'dateFormat' => 'dd.MM.yyyy',
	]);
	?>

	<?php 
	echo DatePicker::widget([
	    'name'  => 'dateTo',
    	'value'  => '',
    	'options' => ['id'=>'dateToInput'],
	    'language' => 'sk',
	    'dateFormat' => 'dd.MM.yyyy',
	]);
	?>

	<input type="submit"/>

</form>

Zvoľ od kedy do kedy

</div>

<div class="col-md-3">
	<div id="cartBox">
		<?php echo \frontend\widgets\CartBox::widget([]); ?>
	</div>
</div>

</div>

<?php $this->registerJsFile('@web/js/cart.js',['depends' => ["frontend\assets\AppAsset"]]); ?>