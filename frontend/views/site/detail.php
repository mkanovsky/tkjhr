<?php 
    use yii\helpers\Url;

    use \frontend\components\Cart;
    use \common\components\Helpers;

    use kartik\growl\GrowlAsset;
    use kartik\base\AnimateAsset;
    
    GrowlAsset::register($this);
    AnimateAsset::register($this);


    $this->title = $item->name.' | Požičovňa hier | TKJHR.sk';
?>

<!-- toto neviem este -->
<input type="hidden" id="dateFromInput" value="<?=$dateFrom;?>"/>
<input type="hidden" id="dateToInput"  value="<?=$dateTo;?>"/>
<!--  -->


<div class="book-detail page">
<div class="container">
    <div class="primary-block clearfix">
        <div class="row">
            <div class="col-sm-4">
                <div class="book-cover book detail-book-cover">


                    <?php 
                        if ($item->detailThumbnailPath) {
                            $thumb = $item->detailThumbnailPath;
                        } else {
                            $thumb = Url::to(['/']).'images/empty-detail.png';
                        }
                    ?>
                    <img src="<?=$thumb;?>" class="img-responsive" alt="">

                    <div class="fade"></div>
                </div><!-- /.book-cover -->
            </div><!-- /.col -->

            <div class="col-sm-8">
                <div class="book-detail-header" style="margin-bottom:12px">
                    <h2 class="book-title"><?=$item->name;?></h2>
                    <p class="book-author">
                        <span class="book-author-name">
                            <?php 

                                $days = Cart::getDays($dateFrom, $dateTo);
                                $word = 'dní';
                                if ($days == 1) $word = 'deň';
                                if ($days == 2) $word = 'dni';
                                if ($days == 3) $word = 'dni';
                                if ($days == 4) $word = 'dni';

                                echo 'Cena je za '.$days.' '.$word;

                                if (Cart::isEvening($dateFrom)) echo ' pri prevzatí vo večerných hodinách (22:00 - 8:00)';
                            ?>
                        </span>
                        </p>
                </div><!-- /.book-detail-header -->

                <div class="book-detail-body">

                     <div class="row" style="margin-bottom:12px">
                        <form id="datechangeform" method="get" action="<?=Url::to(['site/detail']);?>">
                            <input type="hidden" name="id" value="<?=$item->id;?>"/>
                            <div class="col-md-6 col-sm-5">
                                <?php 
                                echo '<label class="control-label">Dátum prevzatia</label>';
                                echo \kartik\datetime\DateTimePicker::widget([
                                    'name' => 'dateFrom',
                                    'type' => \kartik\datetime\DateTimePicker::TYPE_COMPONENT_APPEND,
                                    'value' => $dateFrom,
                                    'options'=>['id'=>'dateFromInput'],
                                    'pluginOptions' => [
                                        'minuteStep' => 30,
                                        'autoclose'=>true,
                                        'format' => 'dd.mm.yyyy hh:ii'
                                    ]
                                ]);

                                ?>
                            </div><!-- /.col -->
                            <div class="col-md-6 col-sm-5">
                                <?php 
                                echo '<label class="control-label">Dátum vrátenia</label>';
                                echo \kartik\datetime\DateTimePicker::widget([
                                    'name' => 'dateTo',
                                    'options'=>['id'=>'dateToInput'],
                                    'type' => \kartik\datetime\DateTimePicker::TYPE_COMPONENT_APPEND,
                                    'value' => $dateTo,
                                    'pluginOptions' => [
                                        'autoclose'=>true,
                                        'format' => 'dd.mm.yyyy hh:ii'
                                    ]
                                ]);

                                ?>
                            </div><!-- /.col -->
                        </form>
                    </div>

                    <div class="detail-cart-button row clearfix">
                        <div class="pull-left col-md-6 col-sm-5 col-xs-12">
                            <div class="detail-book-price">
                                <span class="price">
                                
                                     <?php 
                                       echo Helpers::cuteNumber(Cart::calculateItemPrice($item, $dateFrom, $dateTo, 1));
                                     ?> &euro;

                                </span>
                            </div><!-- /.detail-book-price -->
                        </div><!-- /.pull-left -->

                        <div class="pull-right col-md-6 col-sm-7 col-xs-12">

                            <div class="row product-actions">                            
                                <?php if ($item->isAvailable($dateFromDb, $dateToDb)) { ?>
                                <a href="javascript:void(0)" item-id="<?=$item->id;?>" class="col-sm-6 btn btn-primary btn-addToCart addToCart"><i class="icon-plus fa fa-plus-circle"></i>Do košíka</a>
                                <div class="quantity book-detail-quantity col-xs-4 col-lg-3">  
                                                              
                                </div><!-- /.col -->   
                                <?php } else { ?>
                                <div class="quantity book-detail-quantity col-xs-12">  
                                    <div style="margin-top:10px" class="quantity-block">          
                                        <span class="alert alert-danger">Tovar nie je v uvedenom čase dostupný</span>
                                    </div>
                                </div>
                                <?php } ?>
                            </div><!-- /.row -->    
                        </div><!-- /.pull-right -->
                    </div><!-- /.detail-cart-button -->

                    <div class="clearfix"></div>

                    <div class="product-description">
                        <h3>O hre</h3>
                        <p>
                            <?=$item->text;?>
                        </p>
                    </div><!-- /.product-description -->

                </div><!-- /.book-detail-body -->
            </div><!-- /.col -->
        </div><!-- /.row -->

    </div><!-- /.primary-block -->
</div><!-- /.container -->

</div><!-- /.book-detail page -->
<a class="scrollup hidden-xs hidden-sm" href="#" style="display: inline;"><img src="<?=Url::to(['/']);?>images/top-scroll.png" alt=""></a>

<?php $this->registerJsFile('@web/js/cart.js',['depends' => ["frontend\assets\AppAsset"]]); ?>
