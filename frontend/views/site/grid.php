<?php 
    use yii\helpers\Url;
    
    use \frontend\components\Cart;
    use \common\components\Helpers;

    use kartik\growl\GrowlAsset;
    use kartik\base\AnimateAsset;
    
    GrowlAsset::register($this);
    AnimateAsset::register($this);

    $this->title = ($this->context->rootCId == 1 ? 'Playstation 4' : 'XBOX One').' | Požičovňa hier | TKJHR.sk';

?>
<style>
.book-details .book-title {
    height: 73px;
}

@media screen and (max-width: 660px) {
    #searchBtnDiv {
        margin:auto;
        padding-top:40px;
        text-align:center;
    }
}

</style>
<div class="category page">
    <div class="container">
        <div class="page-header category-page-header">
            <h2 class="page-title"><?=$this->context->rootCId == 1 ? 'Playstation 4' : 'XBOX One';?></h2>
            <p class="page-subtitle">

                    Cena je za 

                    <?php 
                    $days = Cart::getDays($dateFrom, $dateTo);
                    $word = 'dní';
                    if ($days == 1) $word = 'deň';
                    if ($days == 2) $word = 'dni';
                    if ($days == 3) $word = 'dni';
                    if ($days == 4) $word = 'dni';

                    echo $days.' '.$word;
                    if (Cart::isEvening($dateFrom)) echo ' pri prevzatí vo večerných hodinách (22:00 - 8:00)';
                    ?>
            </p>
        </div>

        <div class="page-body">
            <div class="row">
                <!-- ========================================= CONTENT ========================================= -->
                <div class="col-sm-8 col-sm-push-4">
                    <div class="category-toolbar">
                        <div class="row">
                            <form id="datechangeform" method="get">
                            <input type="hidden" name="cId" value="<?=$cId;?>"/>
                            <div class="col-md-5 col-sm-5">
                                <?php 
                                echo '<label class="control-label">Dátum prevzatia</label>';
                                echo \kartik\datetime\DateTimePicker::widget([
                                    'name' => 'dateFrom',
                                    'type' => \kartik\datetime\DateTimePicker::TYPE_COMPONENT_APPEND,
                                    'value' => $dateFrom,
                                    'options'=>['id'=>'dateFromInput'],
                                    'pluginOptions' => [
                                        'minuteStep' => 30,
                                        'autoclose'=>true,
                                        'format' => 'dd.mm.yyyy hh:ii'
                                    ]
                                ]);

                                ?>
                            </div><!-- /.col -->


                            <div class="col-md-5 col-sm-5">
                                <?php 
                                echo '<label class="control-label">Dátum vrátenia</label>';
                                echo \kartik\datetime\DateTimePicker::widget([
                                    'name' => 'dateTo',
                                    'options'=>['id'=>'dateToInput'],
                                    'type' => \kartik\datetime\DateTimePicker::TYPE_COMPONENT_APPEND,
                                    'value' => $dateTo,
                                    'pluginOptions' => [
                                        'autoclose'=>true,
                                        'format' => 'dd.mm.yyyy hh:ii'
                                    ]
                                ]);

                                ?>
                            </div><!-- /.col -->
                            
                            <div id="searchBtnDiv" class="col-md-2 col-sm-12">
                                <label class="control-label">&nbsp;</label>
                                <input type="submit" class="btn btn-primary" value="Vyhladaj"/>
                            </div>
                            </form>

                        </div><!-- /.row -->
                    </div><!-- /.category-toolbar -->

                    <div class="tab-content">
                        <div role="tabpanel" id="grid" class="tab-pane active">
                            <div class="category-books books grid-view">
                                <div class="row">
                                        

                                    <?php foreach ($itemsProvider->getModels() as $model) { ?>
                                    <div class="col-md-4 col-sm-6">

                                        <div class="book">      
                                            <div class="book-cover">
                                                <div class="book-inner">
                                                    <?php 
                                                        if ($model->thumbnailPath) {
                                                            $thumb = $model->thumbnailPath;
                                                        } else {
                                                            $thumb = Url::to(['/']).'images/empty-detail.png';
                                                        }
                                                    ?>
                                                    <img width="193" height="261" alt="" src="<?=$thumb;?>">
                                                    <div class="fade"></div>
                                                    <div class="book-price">

                                                        <span class="price">
                                                             <?php 
                                                               echo Helpers::cuteNumber(Cart::calculateItemPrice($model, $dateFrom, $dateTo, 1));
                                                             ?> &euro;
                                                             
                                                        </span>

                                                    </div><!-- /.book-price -->

                                                    <?php if ($model->isAvailable($dateFromDb, $dateToDb)) { ?>
                                                    <div class="cart animate-effect">
                                                        <div class="action">
                                                            <a title="Add to Cart" href="javascript:void(0);" item-id="<?=$model->id;?>" class="add-to-cart addToCart">
                                                                <i class="icon icon-plus fa fa-plus-circle"></i>
                                                            </a>
                                                            <!-- <a title="Favourite" href="#" class="add-to-cart">
                                                                <i class="icon icon-heart fa fa-heart"></i>
                                                            </a>-->
                                                        </div><!-- /.action -->
                                                    </div><!-- /.cart -->
                                                    <?php } ?>
                                                </div><!-- /.book-inner -->
                                            </div><!-- /.book-cover -->


                                            <div class="book-details">
                                                <h3 class="book-title"><a href="<?=Url::to(['site/detail', 'id'=>$model->id]);?>"><?=$model->name;?></a></h3>
                                                

                                                <p class="book-author">
                                                    <?php if ($model->isAvailable($dateFromDb, $dateToDb)) { ?>
                                                        dostupné
                                                    <?php } else { ?>                                                    
                                                        nedostupné
                                                    <?php } ?>
                                                </p>
                                                
                                                <!--<div class="star-rating">
                                                    <i class="fa fa-star color"></i>
                                                    <i class="fa fa-star color"></i>
                                                    <i class="fa fa-star color"></i>
                                                    <i class="fa fa-star color"></i>
                                                    <i class="fa fa-star "></i>
                                                </div>-->
                                                <!-- /.star-rating -->

                                            </div><!-- /.book-details -->
                                        </div>

                                    </div>
                                    <?php } ?>

                                </div>
                            </div>
                        </div><!-- /.category-books -->
                    </div>

                        <?php 
                            echo \yii\widgets\LinkPager::widget([
                                'pagination'=>$itemsProvider->pagination,
                                'options' => ['class'=>'pagination book-pagination'],
                                'nextPageLabel'=>false,
                                'prevPageLabel'=>false,
                            ]);
                        ?>
                    </div>
                <!-- ========================================= SIDEBAR ========================================= -->
                <?php echo $this->context->renderPartial('_leftMenu', ['cId'=>$cId]);?>
                <!-- ========================================= SIDEBAR :END ========================================= -->
                
                </div>

            </div><!-- /.row -->
        </div><!-- /.page-body -->
    </div><!-- /.container -->
</div><!-- /.category page -->

<a class="scrollup hidden-xs hidden-sm" href="#" style="display: none;"><img src="<?=Url::to(['/']);?>images/top-scroll.png" alt=""></a>
<?php $this->registerJsFile('@web/js/cart.js',['depends' => ["frontend\assets\AppAsset"]]); ?>
