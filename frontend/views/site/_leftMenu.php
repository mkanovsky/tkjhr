<?php 
    use yii\helpers\Url;
    use common\models\ItemType;
?>
<div class="col-sm-4 col-sm-pull-8">
    <aside class="sidebar">
        <!-- ========================================= BOOK CATEGORY ========================================= -->
        <section class="module">
            <header class="module-header">
                <h4 class="module-book-category-title">Kategórie</h4>
            </header><!-- /.module-header -->

            <div class="module-body category-module-body">
                    
                <?php 
                    $rootNode = ItemType::findOne($this->context->rootCId);

                    $rootName = $rootNode->id == 1 ? 'playstation' : 'xbox';
                ?>

                <ul class="list-unstyled category-list">
                    

                    <?php foreach ($rootNode->children(1)->all() as $n=>$child) { ?>

                        <?php 
                            $subChildren = $child->children(1)->all();
                        ?>

                        <li class="<?=$cId == $child->id ? 'active' : '';?> <?=!empty($subChildren) ? 'sub-category-list':'';?>">
                            
                            <?php if ($cId != $child->id) { ?><a href="<?=Url::to(['site/'.$rootName, 'cId'=>$child->id]);?>"><?php } ?>
                                <?=$child->name;?>
                            <?php if ($cId != $child->id) { ?></a><?php } ?>

                            <?php if (!empty($subChildren)) { ?>
                                <ul class="list-unstyled">
                                    <?php foreach ($subChildren as $n=>$subChild) { ?>
                                        <li class="<?=$cId == $subChild->id ? 'active' : '';?>">

                                            <?php if ($cId != $subChild->id) { ?><a href="<?=Url::to(['site/'.$rootName, 'cId'=>$subChild->id]);?>"><?php } ?>
                                                <?=$subChild->name;?>
                                            <?php if ($cId != $subChild->id) { ?></a><?php } ?>

                                        </li>
                                    <?php } ?>
                                </ul>
                            <?php } ?>
                        </li>

                    <?php } ?>


                    <!-- <li><a href="category.html">PS Vita</a></li> -->
                    <!-- <li><a href="category.html">Iné</a></li> -->
                </ul><!-- /.list-unstyled -->
            </div><!-- /.module-body -->
        </section><!-- /.module -->
        <!-- ========================================= BOOK CATEGORY : END ========================================= -->                    
    </aside><!-- /.sidebar -->
</div><!-- /.col -->