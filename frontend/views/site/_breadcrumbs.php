<?php 
    use yii\helpers\Url;
?>

<div class="breadcrumb-container">
	<div class="container">
		<ul class="breadcrumb pull-left">
			<li><a href="<?=Url::to(['/']);?>">Domov</a></li>
			<li><a href="<?=Url::to(['site/playstation']);?>">Playstation</a></li>
		</ul><!-- /.breadcrumb -->

		<!-- ========================================= BREADCRUMB SEARCH BOX ========================================= -->
		<ul class="list-unstyled search-box pull-right">
			<li data-target="#search" data-toggle="sub-header"><button type="button" class="btn btn-primary-dark search-button"><i class="fa fa-search icon"></i></button>
				<div class="row search-action sub-header" id="search">
				    <div class="col-sm-8 col-xs-12 no-padding-right">
				        <div class="input-group">
				            <span class="input-group-btn"><button class="btn btn-search" type="button"><i class="fa fa-search icon"></i></button></span>
				            <input type="text" class="form-control search-book" placeholder="Vyhladaj">
				        </div><!-- /.input-group -->
				    </div><!-- /.col -->
				    
				</div><!-- /.row -->
			</li>
		</ul><!-- /.search-box -->
		<!-- ========================================= BREADCRUMB SEARCH BOX : END ========================================= -->        

	</div><!-- /.container -->
</div><!-- /.breadcrumb-container -->