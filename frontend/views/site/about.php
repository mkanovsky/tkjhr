<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */


$this->title = 'O nás'.' | Požičovňa hier | TKJHR.sk';

use yii\helpers\Html;
?>

<div class="container">
    <div class="row">
    	
    	<div class="col-md-12">
	    	<div class="page-header book-detail-header">
	    		<h1 class="book-title">O nás</h1>
	    	</div>

	    	<div class="page-body">

		    	<div class="book-detail-header">
		    		<p>
			    		Sme prvá požičovňa herných konzol, videohier a herného príslušenstva na Slovensku fungujúca od roku 2016. Okrem prenájmu herného sortimentu, máte odteraz aj možnosť prísť si zahrať obľúbené hry do baru na Obchodnej 39, alebo sa zúčastniť rôznych turnajov a športových / filmových prenosov. Vďaka našim službám už viac nemusíte večer zaspať od nudy, ani ísť na chatu či hotel bez hernej konzoly a tých najlepších hier. Taká je hra.
		    		</p>
		            
	        	</div>
	    	</div>	
    	</div>
    </div>      
</div>
