<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>

<div class="container">
    <div class="center-block  not-found-page-content">
        <div class="page-header">
            <h2 class="page-title">HUPS, <?= nl2br(Html::encode($message)) ?></h2>
            <p class="page-subtitle"><?=$this->title;?></p>
        </div><!-- /.page-header -->
    </div><!-- /.col -->            
</div>