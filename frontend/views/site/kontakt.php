<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */


$this->title = 'Kontakt'.' | Požičovňa hier | TKJHR.sk';

use yii\helpers\Html;
?>

<div class="contact page">
    <div class="container">
        <div class="page-header">
            <h2 class="page-title"> Kontakt </h2>
            <p class="page-subtitle">TAKÁ JE HRA, s.r.o.</p>
        </div><!-- /.page-header -->
    </div><!-- /.container -->

    <div class="map">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2662.0395230970366!2d17.109124351412262!3d48.148043258308576!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x476c8944d982cc53%3A0x49f8a1cc10b30ff!2sObchodn%C3%A1+560%2F39%2C+811+06+Bratislava!5e0!3m2!1ssk!2ssk!4v1469015741348" style="width:100%;"></iframe>
    </div><!-- /.map -->

    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <h3 class="section-title">Adresa</h3>
                
                <p class="section-subtitle">
                
                Obchodná 39<br/>
                Bratislava 811 06 <br/>
                <br/>
                IČO: 50 389 157 <br/>
                <br/>
                Mail: <a href="mailto:tkjhr@tkjhr.sk">tkjhr@tkjhr.sk</a><br/>
                <br/>
                Telefón: 0910 888 420
                <br/>
                
                </p>


                <!--<p class="section-subtitle margin-bottom-sm">Do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
                <p class="section-subtitle">+(01)-234-5578-910</p>-->
            </div><!-- /.col -->

            <div class="col-sm-6">
                <h3 class="section-title">Otváracie hodiny</h3>
                <p class="section-subtitle margin-bottom-sm">

                    Games zone – Obchodná 39:<br/>
                    po-ne  10:00 – 22:00<br/>
                    <br/>
                    Rozvoz:<br/>
                    po-ne  22:00 – 8:00<br/>
                </p>
            </div>

            <!--<div class="col-sm-6">
                <!--<h3 class="section-title">Contact Form</h3>
                <p class="section-subtitle margin-bottom-sm">Do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
                <form class="clearfix" role="form">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="input-group margin-bottom-sm">
                                <span class="input-group-addon input-group-addon-book">
                                    <i class="fa fa-user fa-fw"></i>
                                </span>
                                <input type="text" placeholder="Youremail@email.com" class="form-control form-control-book">
                            </div>
                        </div>
                        <div class="col-sm-6"></div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="input-group margin-bottom-sm">
                                <span class="input-group-addon input-group-addon-book"><i class="fa fa-envelope-o fa-fw"></i></span>
                                <input type="text" placeholder="Youremail@email.com" class="form-control form-control-book">
                            </div>
                        </div>
                        <div class="col-sm-6"></div>
                    </div>

                    <div class="form-group">
                        <textarea placeholder="Your message" rows="10" class="form-control form-control-book"></textarea>
                    </div>
                </form> -->
            </div>
        </div><!-- /.row -->
    </div><!-- /.container -->
</div>