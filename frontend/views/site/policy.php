<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Url;
use yii\helpers\Html;

$this->title = 'Obchodné podmienky'.' | Požičovňa hier | TKJHR.sk';
?>

<div class="container">
    <div class="row">
    	<div class="page-header book-detail-header">
    		<h1 class="book-title">Obchodné podmienky</h1>
    	</div>

    	<div class="page-body">

	    	<div class="book-detail-header">
	            <p class="book-author">Podmienky prenájmu sú:</p>
	            <!-- <br/> -->
	    		<p>
		    		<ul class="list-unstyled list-kybook uppercase">
			    		<li>
			    			Minimálna doba prenájmu 24 hodín (záväzná doba prenájmu sa zadáva pri objednávke cez stránku www.tkjhr.sk, pri telefonickej objednávke zadaním personálom spoločnosti TAKÁ JE HRA s.r.o. a následne vyjadrením súhlasu formou podpisu zákazníka pri preberacej zmluve – tá sa môže predĺžiť v prípade kontaktovania požičovne najneskôr hodinu pred uplynutou lehotou. V prípade nedodržania termínu má požičovňa právo na pokutu z omeškania a následné vymáhanie na základe obchodných podmienok).
			    		</li>

			    		<li>
							Uhradenie sumy za prenájom ihneď po prevzatí (v prípade omeškania navrátenia produktov vzniká za každý omeškaný deň pokuta 10 Eur za ovládač, 15 Eur za hru, 30 Eur za konzolu). Po objednaní je množstvo tovaru a doba požičania pripísaná k používateľovmu kontu na stránke www.tkjhr.sk – čím vzniká záväzok navrátiť rovnaké množstvo tovaru za rovnakú dobu v rovnakom stave ako pri prevzatí.
						</li>

						<li>
							Navrátiť zapožičaný tovar je zákazník povinný vždy do uplynutia lehoty zapožičania (24 / 48 / 72...hodín) do prevádzky na adrese Obchodná 39. Po uplynutí zapožičanej lehoty sa začína účtovať začatý omeškaný deň.
						</li>

						<li>
							Návratnosť produktov v rovnakom stave, ako pri prevzatí z požičovne - nahradenie pôvodnej sumy produktu uvedenej na fakturačných dokladoch v prípade fyzického poškodenia konzol a ovládačov (zjavné fyzické poškodenie produktov, respektíve uvedenie do stavu nefunkčnosti). V prípade hier nahradením poškodenej hry (zjavné fyzické poškodenie, respektíve uvedenie do stavu nefunkčnosti) sumou.
						</li>

						<li>
							Zákazník súhlasí s odovzdaním osobných údajov z Občianskeho preukazu pri preberaní tovaru (celé meno, adresa, číslo občianskeho preukazu - pre vymáhateľnosť prípadnej vzniknutej škody spoločnosti TAKÁ JE HRA s.r.o.). Spoločnosť ručí za neposkytovanie ich tretej strane.
						</li>
		    		</ul>
	    		</p>


	    		<br/>
	    		<p class="book-author">Požičovňa pri prenájme a odovzdaní produktov ručí za:</p>
	    		<!-- <br/> -->
	    		<p>
		    		<ul class="list-unstyled list-kybook uppercase">
			    		<li>100 percentnú funkčnosť produktov</li>
						<li>Prenájom výhradne originálnych a servisovaných produktov</li>
						<li>Chránenie osobných údajov použitých pri registrácii</li>
		    		</ul>
	    		</p>

	    		<br/>
    			<p class="book-author">Všeobecné obchodné podmienky v plnom znení a Reklamačný poriadok si môžete stiahnuť tu:</p>
				<p>
					<ul class="list-unstyled list-kybook uppercase">
			    		<li>
	    					<a class="primary-color" title="Všeobecné obchodné podmienky" href="<?=Url::to(['/']);?>documents/tkjhr_vop.pdf">
	    						<!-- <span class="glyphicon glyphicon-save-file" aria-hidden="true"></span>  -->
	    						Všeobecné obchodné podmienky</a><br/>
    					</li>
    					<li>
							<a title="Reklamačný poriadok" href="<?=Url::to(['/']);?>documents/tkjhr_reklamacny_poriadok.pdf">
								<!-- <span class="glyphicon glyphicon-save-file" aria-hidden="true"></span>  -->
								Reklamačný poriadok</a>
						</li>
					</ul>
				 </p>
        	</div>
    	</div>	
    </div>      
</div>
