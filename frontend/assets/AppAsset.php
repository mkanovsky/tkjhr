<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 
   <!-- Customizable CSS -->
    <link rel="stylesheet" href="assets/css/main.css
    <link rel="stylesheet" href="assets/css/green.css
    <link rel="stylesheet" href="assets/css/bootstrap-select.min.css
    <link rel="stylesheet" href="assets/css/font-awesome.min.css
    <link rel="stylesheet" href="assets/css/owl.carousel.css
    <link rel="stylesheet" href="assets/css/owl.transitions.css
    <link rel="stylesheet" href="assets/css/animate.min.css
    <link rel="stylesheet" href="assets/css/jquery-ui.min.css

    js/bootstrap.min.js
    js/bootstrap-hover-dropdown.min.js
    js/echo.min.js
    js/jquery.easing.min.js
    js/owl.carousel.min.js
    js/wow.min.js
    js/bootstrap-select.min.js
    js/jquery-ui.min.js
    js/scripts.js

    <!-- Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500italic,500,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

    <!-- Icons/Glyphs -->
    <link rel="stylesheet" href="assets/css/elegant-fonts.css">

    <!-- Favicon -->
    <link rel="shortcut icon" href="assets/images/favicon.ico">



 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'css/main.css',
        'css/tkjhr.css',
        'css/bootstrap-select.min.css',
        'css/font-awesome.min.css',
        'css/owl.carousel.css',
        'css/owl.transitions.css',
        'css/animate.min.css',
        'css/jquery-ui.min.css',

    ];
    public $js = [
        'js/bootstrap.min.js',
        'js/bootstrap-hover-dropdown.min.js',
        'js/echo.min.js',
        'js/jquery.easing.min.js',
        'js/owl.carousel.min.js',
        'js/wow.min.js',
        'js/bootstrap-select.min.js',
        'js/jquery-ui.min.js',
        'js/scripts.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
