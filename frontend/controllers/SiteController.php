<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;

use common\components\Helpers;
use common\models\Item;
use common\models\LoginForm;

use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;

/**
 * Site controller
 */
class SiteController extends Controller
{

    public $cId;
    public $rootCId;
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionT() {
        $this->layout = 'empty';

        return $this->render('t', [
            'order'=>\common\models\Order::findOne(1)
        ]);
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionXbox() {
        $cId = isset($_GET['cId']) ? $_GET['cId'] : 5;
        $this->cId = $cId;
        
        $this->rootCId = 2;        

        return $this->renderList($cId);
    }

    public function actionPlaystation() {

        $cId = isset($_GET['cId']) ? $_GET['cId'] : 3;
        $this->cId = $cId;
        $this->rootCId = 1;
        
        return $this->renderList($cId);
    }

    // toto zere datumy s bodkami !
    // ak niekto zada dateFrom neskor ako je dateTo, resp. dateTo skor ako je dateFrom, nastavi dateTo + 1 den po dateFrom
    private function setGlobalDates($dateFrom, $dateTo) {

        $dateFromTime = strtotime(Helpers::formatDateToDb($dateFrom));
        $dateToTime = strtotime(Helpers::formatDateToDb($dateTo));

        if ($dateToTime < $dateFromTime) {
            $datetime = new \DateTime(Helpers::formatDateToDb($dateFrom));
            $datetime->modify('+1 day');
            $dateTo = $datetime->format('d.m.Y H:i');
        }

        Yii::$app->session->set('dateFrom', $dateFrom);
        Yii::$app->session->set('dateTo', $dateTo);
    }

    public function getGlobalDateFrom() {
        if ($dateFrom = Yii::$app->session->get('dateFrom')) return $dateFrom;
        return date('d.m.Y H:i');
    }

    public function getGlobalDateTo() {
        if ($dateTo = Yii::$app->session->get('dateTo')) return $dateTo;
        $datetime = new \DateTime();
        $datetime->modify('+1 day');
        return $datetime->format('d.m.Y H:i');
    }

    public function renderList($cId) {

        if (isset($_GET['dateFrom']) && isset($_GET['dateTo'])) {
            $this->setGlobalDates($_GET['dateFrom'], $_GET['dateTo']);
        }

        $dateFrom = $this->getGlobalDateFrom();
        $dateTo = $this->getGlobalDateTo();

        $itemType = \common\models\ItemType::findOne($cId);
        if ($itemType->isLeaf()) {
            $itemTypeIds = [$itemType->id];
        } else {
            $leaves = $itemType->leaves()->all();
            
            $itemTypeIds = [];
            foreach ($leaves as $l) {
                $itemTypeIds[] = $l->id;
            }
        }

        $dateFromDb = Helpers::formatDateToDb($dateFrom);
        $dateToDb = Helpers::formatDateToDb($dateTo);

        $query = \common\models\Item::find();

        $query->andWhere('item.id IN (
            SELECT itemId FROM item_has_type WHERE itemTypeId IN ('.implode(',', $itemTypeIds).')
        )');

        $query->orderBy('name ASC');
        
        $itemsProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 9, // 3 riadky po 3
            ],
        ]);


        return $this->render('grid', [
            'itemsProvider'=>$itemsProvider,
            'cId'=>$cId,
            
            'dateFrom'=>$dateFrom,
            'dateTo'=>$dateTo,
            'dateFromDb'=>$dateFromDb,
            'dateToDb'=>$dateToDb
        ]);
    }

    
    public function actionDetail($id) {
        $item = Item::findOne($id);

        if (isset($_GET['dateFrom']) && isset($_GET['dateTo'])) {
            $this->setGlobalDates($_GET['dateFrom'], $_GET['dateTo']);
        }

        $dateFrom = $this->getGlobalDateFrom();
        $dateTo = $this->getGlobalDateTo();

        $dateFromDb = Helpers::formatDateToDb($dateFrom);
        $dateToDb = Helpers::formatDateToDb($dateFrom);

        return $this->render('detail', [
            'item'=>$item,

            'dateFrom'=>$dateFrom,
            'dateTo'=>$dateTo,

            'dateFromDb'=>$dateFromDb,
            'dateToDb'=>$dateToDb
        ]);
    }

    public function actionLogout() {
        \Yii::$app->user->logout();
        return $this->redirect(['/']);
    }
    
    public function actionONas() {
        return $this->render('about', [
        ]);
    }    

    public function actionVop() {
        return $this->render('policy', [
        ]);
    }

    public function actionKontakt() {
        return $this->render('kontakt', [
        ]);   
    }
}
