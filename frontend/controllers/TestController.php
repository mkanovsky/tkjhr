<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\data\ActiveDataProvider;
use common\components\Helpers;
/**
 * Site controller
 */
class TestController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionFilterItems($dateFrom = null, $dateTo = null) {

        if ($dateFrom && $dateTo) {

            $dateFrom = Helpers::formatDateToDb($dateFrom);
            $dateTo = Helpers::formatDateToDb($dateTo);

            // dataprovider s 10 strankami a querynou iba na dostupne polozky
            $itemsProvider = new ActiveDataProvider([
                'query' => \common\models\Item::findAllAvailable($dateFrom, $dateTo),
                'pagination' => [
                    'pageSize' => 10,
                ],
            ]);

            return $this->render('filterItems', [
                'itemsProvider'=>$itemsProvider,
                'dateFrom'=>$dateFrom,
                'dateTo'=>$dateTo
            ]);
        } else {
            return $this->render('filterItemsEmpty', [
            ]);
        }

    }

    
}
