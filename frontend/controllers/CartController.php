<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;

use frontend\components\Cart;
use common\models\Order;
use common\models\Item;
/**
 * Cart controller
 */
class CartController extends Controller
{

    public $enableCsrfValidation = false;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionAddItem() {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        extract($_POST);


        Cart::pushItem($itemId, $dateFrom, $dateTo);
        
        $item = Item::findOne($itemId);
        if ($item->isPsKonzola()) {
            Cart::pushItem(Item::DUALSHOCK_ID, $dateFrom, $dateTo);
        }

        if ($item->isXboxKonzola()) {
            Cart::pushItem(Item::XBOXCONTROLLER_ID, $dateFrom, $dateTo);
        }

        $cartItems = \Yii::$app->session->get('cart');

        return [
            'status'=>1,
            'cartItems'=>$cartItems
        ];

        return $cart;
    }

    public function actionRemoveItem() {
        $key = $_POST['cartKey'];

        Cart::removeByKey($key);

        echo json_encode(['status'=>1]);die();
    }

    public function actionCartBox() {
        return \frontend\widgets\CartBox::widget([]);
    }

    public function actionCheckout() {

        if (\Yii::$app->user->isGuest) {
            return $this->render('notLoggedIn', []);
        }
        
        $i = \Yii::$app->user->identity;

        $order = new Order;

        if (!isset($_POST['Order'])) {

            $profile = $i->profile;
            $order->street = $profile->street;
            $order->zip = $profile->zip;
            $order->city = $profile->city;

            $order->street = $profile->street;
            $order->zip = $profile->zip;
            $order->city = $profile->city;

            $order->otherStreet = $profile->otherStreet;
            $order->otherZip = $profile->otherZip;
            $order->otherCity = $profile->otherCity;

        } else {
            $order->setAttributes($_POST['Order']);
            $order->status = 'new';
            $order->userId = $i->id;

            $otherAddress = $order->otherStreet || $order->otherZip || $order->otherCity;
            

            if ($otherAddress && $order->deliveryType == 'courier' && !$order->otherDeliveryPriceId) 
                $order->addError('otherDeliveryPriceId', 'Ak je povinná doručovacia adresa, musí byť toto pole vyplnené');

            if ($otherAddress) {
                $order->deliveryPriceId = $order->otherDeliveryPriceId;
            }


            if ($order->deliveryType == 'courier' && !$order->deliveryPriceId) {
                $order->addError('deliveryPriceId', 'Pole je povinné');
            }

            if ($order->deliveryPriceId) $order->deliveryPrice = \common\models\DeliveryPrice::findOne($order->deliveryPriceId)->amount;


            $stats = Cart::getStats();
            
            $order->dateFrom = $stats['dateFrom'];
            $order->dateTo = $stats['dateTo'];

            $order->dateAdded = date('Y-m-d H:i:s');
            // vd($order->errors);
            

            if ($order->validate(null, false)) {

                return $this->render('confirm', [
                    'order'=>$order
                ]);

                $order->save();
                Cart::setOrderItems($order);
                $order->updateTotalPrice();
                Cart::reset();

                return $this->redirect(['/cart/thankyou']);
            } else {
                // this should not happen
                // vd($order->errors);
            }
        }

        return $this->render('checkout', [
            'order'=>$order,
            'otherAddress' => $order->otherStreet || $order->otherZip || $order->otherCity
            // 'cartContent'=> \frontend\components\Cart::getContent()
        ]);
    }


    public function actionSubmit() {
        if (isset($_POST['Order'])) {
            $order = new Order;
            $order->setAttributes($_POST['Order']);

            $order->save();
            Cart::setOrderItems($order);
            $order->updateTotalPrice();
            Cart::reset();

            $order->sendMail($order->user->email);
            
            return $this->redirect(['/cart/thankyou']);
        }
    }

    public function actionThankyou() {

        return $this->render('thankyou', []);

    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index', [
            'cartContent'=> \frontend\components\Cart::getContent()
        ]);
    }

    public function actionGetStats() {
        $stats = Cart::getStats();

        echo json_encode($stats);
        die();
    }

    public function actionOrder() {

        $order = new Order;

        return $this->render('order', [
            'user'=>\Yii::$app->user->identity,
            'order'=>$order
        ]);

    }

    
}
