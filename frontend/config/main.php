<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'name' => 'tkjhr.sk',
    'bootstrap' => ['log'],
    'language'=>'sk',
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
	
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],

        'i18n' => [
            'translations' => [
                'user'=>[
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@frontend/messages',
                ],
                'app'=>[
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@frontend/messages',
                ],
                // 'yii'=>[
                //     'class' => 'yii\i18n\PhpMessageSource',
                //     'basePath' => '@frontend/messages',
                // ]
            ],
        ],

        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules'=> [
                '/o-nas'=>'/site/o-nas',
                '/vop'=>'/site/vop',
                '/kontakt'=>'/site/kontakt',
            ]
        ],
        'user' => [
            'identityCookie' => [
                'name'     => '_frontendIdentity',
                'path'     => '/',
                'httpOnly' => true,
            ],
        ],
        'session' => [
            'name' => 'FRONTENDSESSID',
            'cookieParams' => [
                'httpOnly' => true,
                'path'     => '/',
            ],
        ], 

        'view' => [
            'theme' => [
                'pathMap' => [
                    '@dektrium/user/views' => '@app/views/user'
                ],
            ],
        ],
        
    ],
	'modules' => [
        'user' => [
            'class'=>'dektrium\user\Module',

            'modelMap'=>[
                'RegistrationForm'=>'\frontend\models\RegistrationForm',

                // 'User'=>'\frontend\models\User',
            ]

            // following line will restrict access to admin controller from frontend application
            // 'as frontend' => 'dektrium\user\filters\FrontendFilter',
        ],

        'treemanager' =>  [
            'class' => '\kartik\tree\Module',
            // other module settings, refer detailed documentation
        ],

    ],
    'params' => $params,
];
